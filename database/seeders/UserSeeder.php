<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $demoUser = User::create([
            'name'              => 'Admin User',
            'email'             => 'admin@admin.com',
            'password'          => bcrypt('admin123'),
            'email_verified_at' => now(),
            'user_type'         => 0,
            // 'api_token'         => bcrypt('demo@demo'),
        ]);

        $demoUser2 = User::create([
            'name'              => 'Demo User',
            'email'             => 'demo@demo.com',
            'password'          => bcrypt('demo1234'),
            'email_verified_at' => now(),
            'user_type'         => 1,
            // 'api_token'         => bcrypt('admin@demo'),
        ]);
    }
}
