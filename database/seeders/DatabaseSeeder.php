<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            MenuSeeder::class,
            PeriodSeeder::class,
            PossibilitySeeder::class,
            CategorySeeder::class,
            ClusterSeeder::class,
            ImpactSeeder::class,
            ScoreSeeder::class,
            ManagementSeeder::class,
            RiskImpactSeeder::class,
        ]);
    }
}
