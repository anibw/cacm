<?php

namespace Database\Seeders;

use App\Models\Cluster;
use Illuminate\Database\Seeder;

class ClusterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clusters = config('cacm.cluster');
        foreach ($clusters as $cluster) {
            Cluster::create($cluster);
        }
    }
}
