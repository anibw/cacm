<?php

namespace Database\Seeders;

use App\Models\Period;
use Illuminate\Database\Seeder;

class PeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // inisialisasi periode data
        Period::create([
            'period'        => 2021,
            'description'   => 'Tahun anggaran 2021',
            'active'        => 0,
        ]);

        Period::create([
            'period'        => 2022,
            'description'   => 'Tahun anggaran 2022',
            'active'        => 1,
        ]);

        Period::create([
            'period'        => 2023,
            'description'   => 'Tahun anggaran 2023',
            'active'        => 0,
        ]);
    }
}
