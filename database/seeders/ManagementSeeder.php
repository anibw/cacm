<?php

namespace Database\Seeders;

use App\Models\Management;
use Illuminate\Database\Seeder;

class ManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $management_items = config('cacm.lkpp');
        $this->addSubMng($management_items);
    }

    private function addSubMng($mng_items, $parent_id = null, $mng_level = 0)
    {
        foreach ($mng_items as $mng_item) {
            $mng_data = new Management();
            $mng_data->title = $mng_item['title'];
            $mng_data->mng_level = $mng_level;
            $mng_data->parent_id = $parent_id;
            $mng_data->save();

            if (array_key_exists('sub_mng', $mng_item)) {
                $sub_mng = $mng_item['sub_mng'];
                $sub_mng_level = $mng_level + 1;
                $this->addSubMng($sub_mng, $mng_data->id, $sub_mng_level);
            }
        }
    }
}
