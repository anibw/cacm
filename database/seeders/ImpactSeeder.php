<?php

namespace Database\Seeders;

use App\Models\Impact;
use Illuminate\Database\Seeder;

class ImpactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $impacts = config('cacm.impact');
        foreach ($impacts as $impact) {
            Impact::create($impact);
        }
    }
}
