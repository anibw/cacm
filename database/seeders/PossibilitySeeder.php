<?php

namespace Database\Seeders;

use App\Models\Possibility;
use Illuminate\Database\Seeder;

class PossibilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $possibilities = config('cacm.possibility');
        foreach ($possibilities as $possibility) {
            Possibility::create($possibility);
        }
    }
}
