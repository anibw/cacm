<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu_items = config('menu_items');
        $position = 1;
        foreach ($menu_items as $item) {
            if (array_key_exists('section', $item)) {
                $menu_dt = Menu::create([
                    'title'     => $item['section'],
                    'section'   => true,
                    'position'  => $position++
                ]);
            } else {
                $menu_dt = new Menu();
                $menu_dt->title = $item['title'];
                if (array_key_exists('page', $item)) {
                    $menu_dt->page = $item['page'];
                }
                if (array_key_exists('new-tab', $item)) {
                    $menu_dt->new_tab = $item['new-tab'];
                }
                if (array_key_exists('icon', $item)) {
                    $menu_dt->icon = $item['icon'];
                }
                $menu_dt->position = $position++;
                $menu_dt->save();

                if (array_key_exists('submenu', $item)) {
                    $sub_menu = $item['submenu'];
                    $this->addSubMenu($sub_menu, $menu_dt->id);
                }
            }
        }
    }

    private function addSubMenu($menu_items, $parent_id)
    {
        $position = 1;
        foreach ($menu_items as $item) {
            $menu_dt = new Menu();
            $menu_dt->title = $item['title'];
            if (array_key_exists('page', $item)) {
                $menu_dt->page = $item['page'];
            }
            if (array_key_exists('new-tab', $item)) {
                $menu_dt->new_tab = $item['new-tab'];
            }
            if (array_key_exists('icon', $item)) {
                $menu_dt->icon = $item['icon'];
            }
            $menu_dt->parent_id = $parent_id;
            $menu_dt->position = $position++;
            $menu_dt->save();
    
            if (array_key_exists('submenu', $item)) {
                $sub_menu = $item['submenu'];
                $this->addSubMenu($sub_menu, $menu_dt->id);
            }
        }
    }

}
