<?php

namespace Database\Seeders;

use App\Models\Impact;
use App\Models\Possibility;
use App\Models\Score;
use Illuminate\Database\Seeder;

class ScoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $possibilities = Possibility::orderBy('id')->get();
        $impacts = Impact::orderBy('id')->get();
        $priority = config('cacm.priority');
        $_idx = 0;
        foreach ($possibilities as $possibility) {
            $_idy = 0;
            foreach ($impacts as $impact) {
                $priority_value = $priority[$_idx][$_idy];
                $cluster_id = ceil($priority_value / 5);
                Score::create([
                    'possibility_id'   => $possibility->id,
                    'impact_id'         => $impact->id,
                    'score'             => $possibility->level * $impact->level,
                    'priority'          => $priority_value,
                    'cluster_id'        => $cluster_id
                ]);
                $_idy++;
            }
            $_idx++;
        }
    }
}
