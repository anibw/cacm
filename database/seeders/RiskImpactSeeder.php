<?php

namespace Database\Seeders;

use App\Models\RiskImpact;
use Illuminate\Database\Seeder;

class RiskImpactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $risk_impacts = config('cacm.risk_impact');
        foreach ($risk_impacts as $category) {
            $category_id = intval($category['category_id']);
            $impacts = $category['impacts'];
            foreach ($impacts as $impact) {
                $impact_id = intval($impact['impact_id']);
                $mng_levels = $impact['mng_level'];
                foreach ($mng_levels as $mng_level) {
                    $description = $mng_level['description'];
                    $levels = $mng_level['level'];
                    foreach ($levels as $level) {
                        RiskImpact::create([
                            'category_id'   => $category_id,
                            'impact_id'     => $impact_id,
                            'mng_level'     => $level,
                            'description'   => $description
                        ]);
                    }
                }
            }
        }
    }
}
