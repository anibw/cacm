<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->id();
            $table->foreignId('possibility_id')->constrained();
            $table->foreignId('impact_id')->constrained();
            $table->integer('score')->unsigned();
            $table->integer('priority')->unsigned();
            $table->foreignId('cluster_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scores', function (Blueprint $table) {
            $table->dropForeign(['possibility_id', 'impact_id', 'cluster_id']);
        });
        Schema::dropIfExists('scores');
    }
}
