<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiskControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_controls', function (Blueprint $table) {
            $table->id();
            $table->foreignId('risk_profile_id')->constrained();
            $table->foreignId('risk_statement_id')->constrained();
            $table->string('control')->nullable();
            $table->tinyInteger('satisfy')->default(0); //1 -> satisfy; 0 -> not yet
            $table->integer('possibility')->unsigned();
            $table->integer('impact')->unsigned();
            $table->integer('score')->unsigned();
            $table->integer('priority')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('risk_controls', function (Blueprint $table) {
            $table->dropForeign(['risk_profile_id', 'risk_statement_id']);
        });
        Schema::dropIfExists('risk_controls');
    }
}
