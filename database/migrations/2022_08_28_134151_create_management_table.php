<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management', function (Blueprint $table) {
            $table->id();
            $table->string('title', 150)->nullable();
            $table->tinyInteger('mng_level')->default(2);
            $table->bigInteger('parent_id')->nullable();
            $table->bigInteger('owner_id')->nullable();
            $table->string('owner_name', 150)->nullable();
            $table->string('owner_position', 150)->nullable();
            $table->bigInteger('manager_id')->nullable();
            $table->string('manager_name', 150)->nullable();
            $table->string('manager_position', 150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management');
    }
}
