<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_statements', function (Blueprint $table) {
            $table->id();
            $table->foreignId('risk_profile_id')->constrained();
            $table->foreignId('category_id')->constrained();
            $table->string('risk_statement')->nullable();
            $table->string('reason')->nullable();
            $table->integer('possibility')->unsigned();
            $table->integer('impact')->unsigned();
            $table->integer('score')->unsigned();
            $table->integer('priority')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tmp_statements', function (Blueprint $table) {
            $table->dropForeign(['risk_profile_id', 'category_id']);
        });
        Schema::dropIfExists('tmp_statements');
    }
}
