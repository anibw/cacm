<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiskProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_profiles', function (Blueprint $table) {
            $table->id();
            $table->foreignId('period_id')->constrained();
            $table->foreignId('management_id')->constrained();
            $table->tinyInteger('semester')->default(1);
            $table->string('number', 25)->nullable();
            $table->string('activity')->nullable();
            $table->string('indicator')->nullable();
            $table->string('target')->nullable();
            $table->string('goal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('risk_profiles', function (Blueprint $table) {
            $table->dropForeign(['period_id', 'management_id']);
        });
        Schema::dropIfExists('risk_profiles');
    }
}
