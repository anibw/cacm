<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('nip', 15)->nullable();
            $table->string('title', 100)->nullable();
            $table->string('name', 150)->nullable();
            $table->integer('gol_ruang')->unsigned()->nullable();
            $table->string('position', 150)->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('user_type')->default(1); // 0:admin; 1:user
            $table->bigInteger('emp_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('emp_id');
        });
        Schema::dropIfExists('employees');
    }
}
