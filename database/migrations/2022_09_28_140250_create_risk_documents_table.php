<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiskDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_documents', function (Blueprint $table) {
            $table->id();
            $table->enum('doc_type', ['CTRL', 'RESP'])->nullable(); // CTRL : RESP
            $table->enum('data_type', ['IMG', 'DOC'])->nullable(); // IMG : DOC
            $table->foreignId('document_id')->nullable();
            $table->string('original_name')->nullable();
            $table->string('filename')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_documents');
    }
}
