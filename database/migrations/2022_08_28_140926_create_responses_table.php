<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('risk_profile_id')->constrained();
            $table->foreignId('risk_statement_id')->constrained();
            $table->string('response')->nullable();
            $table->string('innovation')->nullable();
            $table->string('innovation_target', 1)->nullable(); //decrease "P" or "I"
            $table->string('resource')->nullable();
            $table->integer('possibility')->unsigned();
            $table->integer('impact')->unsigned();
            $table->integer('score')->unsigned();
            $table->integer('priority')->unsigned();
            $table->timestamp('deadline')->nullable();
            $table->string('responsible')->nullable();
            $table->string('indicator')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('responses', function (Blueprint $table) {
            $table->dropForeign(['risk_profile_id', 'risk_statement_id']);
        });
        Schema::dropIfExists('responses');
    }
}
