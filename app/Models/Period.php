<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $fillable = ['period', 'description'];

    public $timestamps = false;

    public function risk_profiles()
    {
        return $this->hasMany(RiskProfile::class);
    }
}
