<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskDocument extends Model
{
    public static function get_images($doc_type, $doc_id)
    {
        return self::where('data_type', '=', 'IMG')
            ->where('doc_type', '=', $doc_type)
            ->where('document_id', '=', $doc_id)
            ->get();
    }

    public static function get_documents($doc_type, $doc_id)
    {
        return self::where('data_type', '=', 'DOC')
            ->where('doc_type', '=', $doc_type)
            ->where('document_id', '=', $doc_id)
            ->get();
    }
}
