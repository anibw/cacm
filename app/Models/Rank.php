<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    public $timestamps = false;

    public function employee()
    {
        return $this->hasMany(Employee::class, 'gol_ruang', 'kode');
    }
}
