<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpStudy extends Model
{
    public $timestamps = false;

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
