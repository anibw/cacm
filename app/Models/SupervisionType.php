<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupervisionType extends Model
{
    public $timestamps = false;
}
