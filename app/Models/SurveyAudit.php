<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyAudit extends Model
{
    public function management()
    {
        return $this->belongsTo(Management::class);
    }

    public function audit_execution()
    {
        return $this->belongsTo(AuditExecution::class);
    }
}
