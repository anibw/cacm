<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskControl extends Model
{
    protected $fillable = [
        'risk_profile_id', 'risk_statement_id', 'control', 'satisfy', 'possibility', 'impact', 'score', 'priority'
    ];

    public function risk_profile()
    {
        return $this->belongsTo(RiskProfile::class);
    }

    public function risk_statement()
    {
        return $this->belongsTo(RiskStatement::class);
    }
}
