<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditReport extends Model
{
    protected $fillable = ['code', 'title', 'report_no', 'report_date', 'status', 'audit_execution_id', 'supervision_type_id'];

    public function management()
    {
        return $this->belongsTo(Management::class);
    }

    public function supervision_type()
    {
        return $this->belongsTo(SupervisionType::class);
    }

    public function audit_execution()
    {
        return $this->belongsTo(AuditExecution::class);
    }
}
