<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    protected $fillable = ['period_id', 'management_id', 'activity', 'indicator', 'target', 'goal'];

    public function management()
    {
        return $this->belongsTo(Management::class);
    }

    public function period()
    {
        return $this->belongsTo(Period::class);
    }
}
