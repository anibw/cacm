<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditTeam extends Model
{
    public function audit_execution()
    {
        return $this->belongsTo(AuditExecution::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function task_type()
    {
        return $this->belongsTo(TaskType::class);
    }
}
