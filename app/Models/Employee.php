<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['nip', 'title', 'name', 'gol_ruang', 'position', 'auditor_level_id'];

    public function owner()
    {
        return $this->hasOne(Management::class, 'owner_id');
    }

    public function manager()
    {
        return $this->hasOne(Management::class, 'manager_id');
    }

    public function rank()
    {
        return $this->belongsTo(Rank::class, 'gol_ruang', 'kode');
    }

    public function auditor_level()
    {
        return $this->belongsTo(AuditorLevel::class);
    }

    public function emp_studies()
    {
        return $this->hasMany(EmpStudy::class);
    }

    public function emp_tasks()
    {
        return $this->hasMany(EmpTask::class);
    }

    public function statements()
    {
        return $this->hasMany(RiskStatement::class, 'audit_id');
    }
}
