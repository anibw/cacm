<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskImpact extends Model
{
    protected $fillable = ['category_id', 'impact_id', 'mng_level', 'description'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function impact()
    {
        return $this->belongsTo(Impact::class);
    }
}
