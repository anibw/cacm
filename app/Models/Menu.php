<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $with = ['submenu'];

    public function submenu()
    {
        return $this->hasMany('App\Models\Menu', 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Menu', 'parent_id', 'id');
    }

    public static function menuItems()
    {
        $data_items = self::whereNull('parent_id')->orderBy('position', 'asc')->get()->toArray();
        return self::arrayItems($data_items);
    }

    private function arrayItems($items, bool $bullet = true)
    {
        $return_arr = array();

        foreach ($items as $item) {
            $item_arr = array();

            if ($item['section'] == true) {
                $item_arr['section'] = $item['title'];
            } else {
                $item_arr['title'] = $item['title'];
                if (empty($item['parent_id'])) { $item_arr['root'] = true; }
                if (!empty($item['icon'])) { $item_arr['icon'] = $item['icon']; }
                if (!empty($item['page'])) { $item_arr['page'] = $item['page']; }
                $item_arr['new-tab'] = $item['new_tab'];

                if (!empty($item['submenu'])) {
                    $item_arr['bullet'] = $bullet ? 'dot': 'line';
                    $item_arr['submenu'] = self::arrayItems($item['submenu'], !$bullet);
                }
            }
           array_push($return_arr, $item_arr);
        }
        return $return_arr;        
    }

}
