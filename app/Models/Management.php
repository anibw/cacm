<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Management extends Model
{
    protected $fillable = ['title', 'parent_id', 'mng_level', 'owner_name', 'owner_position', 'manager_name', 'manager_position'];

    protected $with = ['sub_mng'];

    public function sub_mng()
    {
        return $this->hasMany('App\Models\Management', 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Management', 'parent_id', 'id');
    }

    public function targets()
    {
        return $this->hasMany(Target::class);
    }

    public function stakeholders()
    {
        return $this->hasMany(Stakeholder::class);
    }

    public function risk_profiles()
    {
        return $this->hasMany(RiskProfile::class);
    }

    public function owner()
    {
        return $this->belongsTo(Employee::class, 'owner_id');
    }

    public function manager()
    {
        return $this->belongsTo(Employee::class, 'manager_id');
    }

    public static function getIncludeID($item_id)
    {
        $include_sub_id = [];
        $data_mng = self::find($item_id)->toArray();
        self::arrayItems($data_mng, $include_sub_id);
        return $include_sub_id;
    }

    private static function arrayItems($item, &$item_arr)
    {
        $item_arr[] = $item['id'];

        if (!empty($item['sub_mng'])) {
            $sub_items = $item['sub_mng'];
            foreach ($sub_items as $sub_item) {
                self::arrayItems($sub_item, $item_arr);
            }
        }
    }

    public static function item_tree($items = null)
    {
        if ($items == null) {
            $items = self::whereNull('parent_id')->get();
        }
        $return_arr = array();

        foreach ($items as $item) {
            $item_arr = array();

            $item_arr['id'] = $item->id;
            $item_arr['text'] = $item->title;
            switch ($item->mng_level) {
                case 0:
                    $item_arr['icon'] = 'fas fa-home text-warning';
                    $item_arr['state'] = ['opened' => true, 'selected' => true];
                    break;
                case 1:
                    $item_arr['icon'] = 'fas fa-home text-primary';
                    break;
                case 2:
                    $item_arr['icon'] = 'fas fa-house-user text-primary';
                    break;
            }

            $item_arr['a_attr'] = [
                'mng_level' => $item->mng_level,
                'owner_id' => $item->owner_id,
                'owner_name' => $item->owner_name,
                'owner_position' => $item->owner_position,
                'manager_id' => $item->manager_id,
                'manager_name' => $item->manager_name,
                'manager_position' => $item->manager_position,
            ];

            if (!empty($item->sub_mng)) {
                $item_arr['children'] = self::item_tree($item->sub_mng);
            }
            array_push($return_arr, $item_arr);
        }
        return $return_arr;
    }

    public static function pie_data($risk_type, $mng_id = 1)
    {
        $period_id = period()->id;
        $mng_sub_id = self::getIncludeID($mng_id);
        $profile = RiskProfile::select('risk_profiles.management_id')
            ->where('risk_profiles.period_id', '=', $period_id)
            ->whereIn('risk_profiles.management_id', $mng_sub_id)
            ->groupBy('risk_profiles.management_id');
        switch ($risk_type) {
            case 'state':
                $profile = $profile->addSelect(DB::raw('COUNT(state.id) AS risk_count'))
                    ->join('risk_statements AS state', 'state.risk_profile_id', '=', 'risk_profiles.id');
                break;
            case 'control':
                $profile = $profile->addSelect(DB::raw('COUNT(control.id) AS risk_count'))
                    ->join('risk_controls AS control', 'control.risk_profile_id', '=', 'risk_profiles.id');
                break;
            case 'respon':
                $profile = $profile->addSelect(DB::raw('COUNT(respon.id) AS risk_count'))
                    ->join('responses AS respon', 'respon.risk_profile_id', '=', 'risk_profiles.id');
                break;
        }

        $pie_data = self::get_pie_data($profile->get(), $mng_id);
        $pie_data['name'] = uniqid();
        $pie_data['title'] = 'Peta Risiko';

        return $pie_data;
    }

    private static function get_pie_data($risk_data, $mng_id)
    {
        $management = self::select('id', 'title', 'parent_id')
            ->without('sub_mng')
            ->where('id', '=', $mng_id)->orWhere('parent_id', '=', $mng_id)
            ->get();

        $data = array();
        $labels = array();
        $management->each(function ($item) use ($risk_data, $mng_id, &$data, &$labels) {
            $risk_count = 0;
            $mng_sub_id = [$mng_id];
            if ($item->id != $mng_id) {
                $mng_sub_id = self::getIncludeID($item->id);
            }
            $risk_data->each(function ($risk_item) use ($mng_sub_id, &$risk_count) {
                if (in_array($risk_item->management_id, $mng_sub_id)) {
                    $risk_count += $risk_item->risk_count;
                }
            });

            $data[] = $risk_count;
            $labels[] = $item->title;
        });

        return ['series' => $data, 'labels' => $labels];
    }
}
