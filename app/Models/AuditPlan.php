<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditPlan extends Model
{
    protected $fillable = ['period_id', 'title', 'is_pkpt', 'status', 'time_plan', 'triwulan', 'audit_type_id', 'supervision_type_id'];

    public function audit_type()
    {
        return $this->belongsTo(AuditType::class);
    }

    public function management()
    {
        return $this->belongsTo(Management::class);
    }

    public function supervision_type()
    {
        return $this->belongsTo(SupervisionType::class);
    }

    public function risk_statement()
    {
        return $this->belongsTo(RiskStatement::class);
    }

    public function audit_execution()
    {
        return $this->hasOne(AuditExecution::class);
    }

    public function update_code()
    {
        $this->code = get_code($this->is_pkpt, $this->audit_type_id, $this->supervision_type_id)
            . '-' . str_pad($this->id, 6, '0', STR_PAD_LEFT);
        $this->update();
    }
}
