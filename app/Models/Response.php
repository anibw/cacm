<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = [
        'risk_profile_id', 'risk_statement_id', 'response', 'innovation', 'innovation_target', 'possibility', 'impact', 'score', 'priority', 'responsible', 'resource', 'deadline', 'indicator'
    ];

    public function risk_profile()
    {
        return $this->belongsTo(RiskProfile::class);
    }

    public function risk_statement()
    {
        return $this->belongsTo(RiskStatement::class);
    }
}
