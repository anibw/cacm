<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskStatement extends Model
{
    protected $fillable = [
        'risk_profile_id', 'category_id', 'risk_statement', 'reason',
        'possibility', 'impact', 'score', 'priority'
    ];

    public function control()
    {
        return $this->hasOne(RiskControl::class);
    }

    public function response()
    {
        return $this->hasOne(Response::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'audit_id');
    }

    public function risk_profile()
    {
        return $this->belongsTo(RiskProfile::class);
    }

    public static function pie_data()
    {
        // $statement = self::select('priority')->selectRaw('COUNT(priority) As score')->groupBy('priority');
        $management = Management::where('mng_level', '=', 2)
            ->leftJoin('risk_profiles AS profil', 'profil.management_id', '=', 'management.id')->get();
        return $management;
    }
}
