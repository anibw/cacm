<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Possibility extends Model
{
    protected $fillable = ['title', 'tolerance', 'percentage', 'frequency'];

    public $timestamps = false;

    public function scores()
    {
        return $this->hasMany(Score::class, 'possibility', 'level');
    }

    public static function full_title()
    {
        return self::select(DB::raw("CONCAT(id,' -> ',title) AS title"), 'id')
            ->pluck('title', 'id')->prepend('-', '');
    }
}
