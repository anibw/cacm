<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stakeholder extends Model
{
    protected $fillable = ['management_id', 'nomor', 'status', 'stakeholder', 'description'];

    public function management()
    {
        return $this->belongsTo(Management::class);
    }

    public function risk_profile()
    {
        return $this->belongsTo(RiskProfile::class);
    }
}
