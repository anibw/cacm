<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    public $timestamps = false;

    public function possibility()
    {
        return $this->belongsTo(Possibility::class);
    }

    public function impact()
    {
        return $this->belongsTo(Impact::class);
    }

    public function cluster()
    {
        return $this->belongsTo(Cluster::class);
    }

    public static function heat_series($data_label = 'score')
    {
        $scores = self::select('scores.possibility_id', 'scores.impact_id', 'scores.priority');
        switch ($data_label) {
            case 'state':
                $statement = RiskStatement::select('priority')->selectRaw('COUNT(priority) As score')->groupBy('priority');
                $scores = $scores->addSelect('statement.score')
                    ->leftJoinSub($statement, 'statement', function ($join) {
                        $join->on('scores.priority', '=', 'statement.priority');
                    });
                break;
            case 'control':
                $control = RiskControl::select('priority')->selectRaw('COUNT(priority) As score')->groupBy('priority');
                $scores = $scores->addSelect('control.score')
                    ->leftJoinSub($control, 'control', function ($join) {
                        $join->on('scores.priority', '=', 'control.priority');
                    });
                break;
            case 'respon':
                $respon = Response::select('priority')->selectRaw('COUNT(priority) As score')->groupBy('priority');
                $scores = $respon->addSelect('respon.score')
                    ->leftJoinSub($respon, 'respon', function ($join) {
                        $join->on('scores.priority', '=', 'respon.priority');
                    });
                break;
            case 'priority':
                $scores = $scores->addSelect('scores.priority AS score');
                break;
            case 'score':
                $scores = $scores->addSelect('scores.score');
                break;
        }

        return [
            'name' => uniqid(),
            'title' => 'Peta Risiko',
            'ranges' => get_ranges(),
            'series' => get_series($scores->get())
        ];
    }
}
