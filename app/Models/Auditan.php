<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auditan extends Model
{
    protected $fillable = ['satker', 'alamat', 'telp', 'fax', 'email'];
}
