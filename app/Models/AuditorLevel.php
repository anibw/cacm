<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditorLevel extends Model
{
    public $timestamps = false;

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
