<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiskProfile extends Model
{
    protected $fillable = ['period_id', 'management_id', 'number', 'semester', 'target', 'indicator', 'activity', 'goal'];

    public function risk_statements()
    {
        return $this->hasMany(RiskStatement::class);
    }

    public function tmp_statements()
    {
        return $this->hasMany(TmpStatement::class);
    }

    public function risk_controls()
    {
        return $this->hasMany(Control::class);
    }

    public function responses()
    {
        return $this->hasMany(Response::class);
    }

    public function period()
    {
        return $this->belongsTo(Period::class);
    }

    public function management()
    {
        return $this->belongsTo(Management::class);
    }
}
