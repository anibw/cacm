<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskType extends Model
{
    public $timestamps = false;

    public function audit_team()
    {
        return $this->hasMany(AuditTeam::class);
    }
}
