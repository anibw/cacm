<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title', 'description'];

    public $timestamps = false;

    public function risk_impacts()
    {
        return $this->hasMany(RiskImpact::class);
    }

    public function risk_statements()
    {
        return $this->hasMany(RiskStatement::class);
    }

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $impacts = Impact::pluck('level');
            foreach ($impacts as $level) {
                for ($i = 0; $i <= 2; $i++) {
                    $model->risk_impacts()->create([
                        'impact_id' => $level,
                        'mng_level' => $i,
                    ]);
                }
            }
        });

        static::deleting(function ($model) {
            $model->risk_impacts()->delete();
        });
    }
}
