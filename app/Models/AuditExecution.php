<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditExecution extends Model
{
    protected $fillable = ['period_id', 'code', 'title', 'is_pkpt', 'status', 'time_plan', 'triwulan', 'audit_type_id', 'supervision_type_id', 'audit_plan_id', 'nomor_st', 'tanggal_st', 'tmt_awal', 'tmt_akhir', 'jml_hari', 'jenis_st', 'status_st'];

    public function audit_type()
    {
        return $this->belongsTo(AuditType::class);
    }

    public function management()
    {
        return $this->belongsTo(Management::class);
    }

    public function supervision_type()
    {
        return $this->belongsTo(SupervisionType::class);
    }

    public function audit_plan()
    {
        return $this->belongsTo(SupervisionType::class);
    }

    public function audit_teams()
    {
        return $this->hasMany(AuditTeam::class);
    }

    public function update_code()
    {
        $this->code = get_code($this->is_pkpt, $this->audit_type_id, $this->supervision_type_id)
            . '-1' . str_pad($this->id, 5, '0', STR_PAD_LEFT);
        $this->update();
    }
}
