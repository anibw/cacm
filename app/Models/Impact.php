<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Impact extends Model
{
    protected $fillable = ['title', 'description'];

    public $timestamps = false;

    public function scores()
    {
        return $this->hasMany(Score::class, 'impact', 'level');
    }

    public function risk_impacts()
    {
        return $this->hasMany(RiskImpact::class);
    }

    public static function full_title()
    {
        return self::select(DB::raw("CONCAT(id,' -> ',title) AS title"), 'id')
            ->pluck('title', 'id')->prepend('-', '');
    }
}
