<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TmpStatement extends Model
{
    protected $fillable = [
        'risk_profile_id', 'category_id', 'risk_statement', 'reason',
        'possibility', 'impact', 'score', 'priority'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function risk_profile()
    {
        return $this->belongsTo(RiskProfile::class);
    }
}
