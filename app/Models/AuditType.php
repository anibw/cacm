<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditType extends Model
{
    public $timestamps = false;
}
