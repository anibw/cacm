<?php

// use Illuminate\Support\Facades\Request;
use App\Models\Cluster;
use App\Models\Impact;
use App\Models\Management;
use App\Models\Period;
use App\Models\Possibility;
use App\Models\RiskImpact;
use App\Models\RiskProfile;
use App\Models\Score;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

// fungsi get data periode yang aktif
function period()
{
    $periode = Period::where('active', '=', 1)->first();
    return $periode;
}

function q_risk_profiles($uker_id)
{
    $period_id = period()->id;
    $mng_id = Management::getIncludeID($uker_id);
    return RiskProfile::where('period_id', '=', $period_id)
        ->whereIn('management_id', $mng_id);
}

function priority($possibility, $impact)
{
    $score = DB::table('scores AS sc')->select('sc.priority')
        ->join('possibilities AS ps', 'ps.id', 'sc.possibility_id')
        ->where('ps.level', '=', $possibility)
        ->join('impacts AS ip', 'ip.id', 'sc.impact_id')
        ->where('ip.level', '=', $impact)
        ->first();
    return $score->priority;
}

function act_button($types = [], $link, $id = '')
{
    if (!empty($types)) {
        $value = '<form action="' . url($link . $id) . '" method="POST" accept-charset="UTF-8" onsubmit="event.preventDefault(); confirm_delete(this);">' . csrf_field() . '<input name="_method" type="hidden" value="DELETE">';
        foreach ($types as $tipe) {
            switch ($tipe) {
                case 'edit':
                    $url = $link . $id . '/edit';
                    $value .= '<a class="btn btn-icon btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Data" href="' . url($url) . '" ><i class="fas fa-pencil-alt"></i></a>&nbsp;';
                    break;

                case 'delete':
                    $value .= '<button type="submit" class="btn btn-sm btn-icon btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Data"><i class="fas fa-trash"></i></button>';
                    break;

                case 'show':
                    $url = $link . $id;
                    $value .= '<a class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" data-placement="top" title="Detail Data" href="' . url($url) . '"><i class="fas fa-scroll"></i></a>&nbsp;';
                    break;

                default:
                    # code...
                    break;
            }
        }
        $value .= '</form>';
    }
    return $value;
}

function act_menu($types = [], $link, $id = '')
{
    if (!empty($types)) {
        $value = '<div class="dropdown dropdown-inline">
                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">
                        <i class="fas fa-cogs"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                        <ul class="navi flex-column navi-hover py-2">';

        foreach ($types as $tipe) {
            switch ($tipe) {
                case 'edit':
                    $url = $link . $id . '/edit';
                    $value .=  '<li class="navi-item">
                                    <a href="' . url($url) . '" class="navi-link">
                                        <span class="navi-icon"><i class="fas fa-edit"></i></span>
                                        <span class="navi-text">Edit</span>
                                    </a>
                                </li>';
                    break;

                case 'delete':
                    $value .= '<li class="navi-item">
                                    <a href="#" onclick="actDelete(' . $id . ');" class="navi-link">
                                        <span class="navi-icon"><i class="fas fa-trash-alt"></i></span>
                                        <span class="navi-text">Delete</span>
                                    </a>
                                </li>';
                    break;

                case 'show':
                    $url = $link . $id;
                    $value .=  '<li class="navi-item">
                                    <a href="' . url($url) . '" class="navi-link">
                                        <span class="navi-icon"><i class="fas fa-scroll"></i></span>
                                        <span class="navi-text">Detail</span>
                                    </a>
                                </li>';
                    break;

                default:
                    # code...
                    break;
            }
        }
        $value .= '</ul></div></div>';
    }
    return $value;
}

function show_message($type)
{
    switch ($type) {
        case 'save':
            $value = 'Data berhasil disimpan!';
            break;
        case 'update':
            $value = 'Data berhasil diubah';
            break;
        case 'delete':
            $value = 'Data berhasil dihapus';
            break;
        case 'reset':
            $value = '%s berhasil direset';
            break;
        default:
            $value = 'unknown';
    }
    return $value;
}

function on_checked($id, $active = true)
{
    if ($active) {
        $value = '<span class="switch switch-icon"><label><input onclick="event.preventDefault();" type="checkbox" checked name="select"/><span></span></label></span>';
    } else {
        $value = '<span class="switch switch-icon"><label><input onclick="event.preventDefault(); set_active(' . $id . ');" type="checkbox" name="select"/><span></span></label></span>';
    }
    return $value;
}

function get_risk_impact($category, $level)
{
    return RiskImpact::select('impact_id', 'category_id', 'description')
        ->where('mng_level', '=', $level)->where('category_id', '=', $category);
}

function get_management_level($level)
{
    switch ($level) {
        case 0:
            return 'LKPP';
        case 1:
            return 'UPR TK-1';
        case 2:
            return 'UPR TK-2';
    }
}

function risk_cluster($possibility, $impact)
{
    $score = Score::where('possibility_id', '=', $possibility)
        ->where('impact_id', '=', $impact)
        ->with('cluster')
        ->first();

    return $score;
}

function btn_risk_cluster($possibility, $impact, $full_title = false)
{
    $cluster = risk_cluster($possibility, $impact)->cluster;
    if ($full_title) {
        $title = $cluster->name;
    } else {
        $title = preg_replace('/(?<=\w)./', '', $cluster->name);
    }
    $button = '<button type="button" class="btn btn-sm text-white font-weight-bold" data-toggle="tooltip" data-placement="top" title="' . $cluster->name . '" style="background-color:' . $cluster->color . '">' . $title  . '</button>';

    return Str::of($button)->toHtmlString();
}

function innovation_target($target)
{
    $button = '<button type="button" class="btn btn-sm text-white font-weight-bold" data-toggle="tooltip" data-placement="top" style="background-color:blue"><i class="fas fa-arrow-down"></i>' . $target . '</button>';

    return Str::of($button)->toHtmlString();
}

function get_series($scores)
{
    $arr_series = [];
    $scores = $scores->groupBy('possibility_id');
    foreach ($scores as $key => $impacts) {
        $item_arr = [];
        $item_arr['name'] = $key;
        $data = [];
        foreach ($impacts as $score) {
            $data[] = [
                'x' => 'D-' . $score->impact_id,
                'y' => $score->priority,
                'score' => $score->score ?? ''
            ];
        }
        $item_arr['data'] = $data;
        array_push($arr_series, $item_arr);
    }
    return $arr_series;
}

function get_ranges()
{
    return Cluster::select('from', 'to', 'name', 'color')->get();
}

function lbl_possibility($value)
{
    $str_possibility = Possibility::find($value)->title;
    $str_possibility = '<span style="cursor:pointer" title="' . $str_possibility . '">' . $value . '</span>';
    return Str::of($str_possibility)->toHtmlString();
}

function lbl_impact($value)
{
    $str_impact = Impact::find($value)->title;
    $str_impact = '<span style="cursor:pointer" title="' . $str_impact . '">' . $value . '</span>';
    return Str::of($str_impact)->toHtmlString();
}

function gol_ruang($value)
{
    $gol = str_gol(intdiv($value, 10));
    $ruang = str_ruang($value % 10);
    return $gol . '/' . $ruang;
}

function str_gol($var)
{
    switch ($var) {
        case 1:
            return 'I';
        case 2:
            return 'II';
        case 3:
            return 'III';
        case 4:
            return 'IV';
    }
}

function str_ruang($var)
{
    switch ($var) {
        case 1:
            return 'A';
        case 2:
            return 'B';
        case 3:
            return 'C';
        case 4:
            return 'D';
        case 5:
            return 'E';
    }
}

function status_penugasan($status)
{
    switch ($status) {
        case 0:
            return 'DALAM PROSES';
        case 1:
            return 'SELESAI';
        case 2:
            return 'PERPANJANGAN';
    }
}

function get_month()
{
    return [
        1 => 'Januari', 2 => 'Pebruari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni',
        7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'
    ];
}

function get_triwulan($month)
{
    $triwulan = intdiv($month, 3);
    if (($month % 3) != 0) {
        $triwulan++;
    }

    return $triwulan;

    // return [1 => 'TW 1', 2 => 'TW 2', 3 => 'TW 3', 4 => 'TW 4'];
}

function get_status_penugasan()
{
    return [0 => 'Dalam Proses', 1 => 'Selesai', 2 => 'Perpanjangan'];
}

function get_jenis_st()
{
    return [0 => 'ST Awal', 1 => 'ST Perpanjangan 1', 2 => 'ST Perpanjangan 2', 3 => 'ST Perpanjangan 3', 4 => 'ST Perpanjangan 4'];
}

function get_code($is_pkpt, $audit_type, $supervision_type)
{
    $str_return = ($is_pkpt == 1) ? 'P' : 'N';
    $str_return .= '-' . get_audit_type($audit_type);
    $str_return .= '-0' . $supervision_type;
    return $str_return;
}

function get_audit_type($audit_type)
{
    switch ($audit_type) {
        case 1:
            return 'A';
        case 2:
            return 'B';
        case 3:
            return 'C';
    }
}
