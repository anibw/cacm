<?php

namespace App\Http\Controllers;

use App\Models\Management;
use App\Models\Score;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page_title = 'Dashboard';
        $page_description = 'Dashboard SIM CACM';
        $data_type = 'state';
        $heat_data_state = Score::heat_series($data_type);
        $pie_data_state = Management::pie_data($data_type);
        $data_type = 'control';
        $heat_data_control = Score::heat_series($data_type);
        $pie_data_control = Management::pie_data($data_type);

        // return $pie_data;
        return view('pages.dashboard', compact('page_title', 'page_description', 'heat_data_state', 'pie_data_state', 'heat_data_control', 'pie_data_control'));
    }
}
