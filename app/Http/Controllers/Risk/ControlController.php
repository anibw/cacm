<?php

namespace App\Http\Controllers\Risk;

use App\Http\Controllers\Controller;
use App\Models\Impact;
use App\Models\Management;
use App\Models\Possibility;
use App\Models\RiskControl;
use App\Models\RiskStatement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ControlController extends Controller
{
    private $page_title = 'Pengendalian Risiko';

    private function rules(Request $request, bool $isEdit)
    {
        $rules = [
            'control' => 'required|min:5|max:250',
            'possibility' => 'integer',
            'impact' => 'integer',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $uker_id = 1;
        if (isset($request->uker_id)) {
            $uker_id = $request->uker_id;
        }
        $page_title = $this->page_title;
        $page_description = 'list data -> Periode ' . period()->period;
        $uker = Management::pluck('title', 'id');

        return view('pages.risk.control.index', compact('page_title', 'page_description', 'uker', 'uker_id'));
    }

    /**
     * Show the list of targets data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $risk_profiles = q_risk_profiles($request->uker_id);

        $data = RiskControl::select('risk_controls.id', 'risk_controls.risk_statement_id', 'risk_controls.control', 'risk_controls.satisfy', 'risk_controls.possibility', 'risk_controls.impact', 'risk_controls.priority')
            ->joinSub($risk_profiles, 'profiles', function ($join) {
                $join->on('profiles.id', '=', 'risk_controls.risk_profile_id');
            })->orderBy('risk_controls.risk_statement_id', 'asc')
            ->get();

        return DataTables::of($data)
            ->editColumn('possibility', function ($data) {
                return lbl_possibility($data->possibility);
            })
            ->editColumn('impact', function ($data) {
                return lbl_impact($data->impact);
            })
            ->addColumn('action', function ($data) {
                return act_button(['edit', 'delete'], '/risk/control/', $data->id);
            })
            ->addColumn('risk_statement', function ($data) {
                return $data->risk_statement->risk_statement;
            })
            ->addColumn('st_possibility', function ($data) {
                return lbl_possibility($data->risk_statement->possibility);
            })
            ->addColumn('st_impact', function ($data) {
                return lbl_impact($data->risk_statement->impact);
            })
            ->addColumn('st_risk', function ($data) {
                return btn_risk_cluster($data->risk_statement->possibility, $data->risk_statement->impact);
            })
            ->addColumn('risk_level', function ($data) {
                return btn_risk_cluster($data->possibility, $data->impact);
            })
            ->addColumn('memadai', function ($data) {
                if ($data->satisfy == 1) {
                    return 'Memadai';
                } else {
                    return 'Belum';
                }
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $risk_statement = RiskStatement::whereIn('id', $request->statement_id)->get();

        foreach ($risk_statement as $statement) {
            RiskControl::create([
                'risk_profile_id' => $statement->risk_profile_id,
                'risk_statement_id' => $statement->id,
                'possibility' => $statement->possibility,
                'impact' => $statement->impact,
                'score' => $statement->score,
                'priority' => $statement->priority
            ]);
        }

        return redirect(route('control.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = RiskControl::find($id);
        $possibilities = Possibility::full_title();
        $impacts = Impact::full_title();

        return view('pages.risk.control.form', compact('page_title', 'page_description', 'data', 'possibilities', 'impacts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $images = $request->file('images');
        // dd($request->all(), $images);
        $validator = Validator::make($request->all(), $this->rules($request, false));
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Pengendalian Risiko tidak boleh kosong');
        }

        $update = RiskControl::find($id);
        $update->fill($request->all());
        $update->update();
        $uker_id = $update->risk_profile->management->parent->id ?? 1;

        return redirect(route('control.index', ['uker_id' => $uker_id]))->with('success', show_message('save'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = RiskControl::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
