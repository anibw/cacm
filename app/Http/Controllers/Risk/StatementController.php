<?php

namespace App\Http\Controllers\Risk;

use App\Http\Controllers\Controller;
use App\Models\AuditPlan;
use App\Models\Category;
use App\Models\Impact;
use App\Models\Management;
use App\Models\Possibility;
use App\Models\Response;
use App\Models\RiskControl;
use App\Models\RiskProfile;
use App\Models\RiskStatement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Datatables;

class StatementController extends Controller
{
    private $page_title = 'Pernyataan Risiko';

    private function rules(Request $request, bool $isEdit)
    {
        $rules = [
            'risk_statement' => 'required|min:5|max:250',
            'reason' => 'min:5|max:250',
            'possibility' => 'integer',
            'impact' => 'integer',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $uker_id = 1;
        if (isset($request->uker_id)) {
            $uker_id = $request->uker_id;
        }
        $page_title = $this->page_title;
        $page_description = 'list data -> Periode ' . period()->period;
        $uker = Management::pluck('title', 'id');

        return view('pages.risk.statement.index', compact('page_title', 'page_description', 'uker', 'uker_id'));
    }

    /**
     * Show the list of targets data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $period_id = period()->id;
        $uker_id = $request->uker_id;
        $mng_id = Management::getIncludeID($uker_id);

        $data = DB::table('risk_statements AS st')
            ->select('st.id', 'pf.goal', 'st.risk_statement', 'cat.title AS category', 'st.reason', 'st.possibility', 'st.impact', 'st.score', 'st.priority')
            ->leftJoin('categories AS cat', 'st.category_id', 'cat.id')
            ->join('risk_profiles AS pf', 'pf.id', 'st.risk_profile_id')
            ->where('pf.period_id', '=', $period_id)
            ->whereIn('pf.management_id', $mng_id);

        if (isset($request->risk_from)) {
            $risk_from = $request->risk_from;
            $statement_id_control = RiskControl::pluck('risk_statement_id');
            if ($risk_from == 'control') {
                $data = $data->whereNotIn('st.id', $statement_id_control);
            } else if ($risk_from == 'respon') {
                $statement_id_respon = Response::pluck('risk_statement_id');
                $data = $data->whereIn('st.id', $statement_id_control);
                $data = $data->whereNotIn('st.id', $statement_id_respon);
            } else if ($risk_from == 'audit_plan') {
                $statement_id_plan = AuditPlan::pluck('risk_statement_id')->whereNotNull();
                $data = $data->whereNotIn('st.id', $statement_id_plan);
            }
        }

        $data = $data->orderBy('pf.id', 'asc')->orderBy('st.id', 'asc')->get();

        return Datatables::of($data)
            ->editColumn('possibility', function ($data) {
                return lbl_possibility($data->possibility);
            })
            ->editColumn('impact', function ($data) {
                return lbl_impact($data->impact);
            })
            ->addColumn('risk_level', function ($data) {
                return btn_risk_cluster($data->possibility, $data->impact);
            })
            ->addColumn('action', function ($data) {
                return act_button(['edit', 'delete'], '/risk/statement/', $data->id);
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function tmp_list(Request $request)
    {
        $data = [];
        if (isset($request->profile_id)) {
            $profile_id = $request->profile_id;
            try {
                $data = RiskProfile::findOrFail($profile_id);
                $data = $data->tmp_statements;
            } catch (\Throwable $th) {
                $data = [];
            }
        };

        return Datatables::of($data)
            ->editColumn('possibility', function ($data) {
                return lbl_possibility($data->possibility);
            })
            ->editColumn('impact', function ($data) {
                return lbl_impact($data->impact);
            })
            ->addColumn('risk_level', function ($data) {
                return btn_risk_cluster($data->possibility, $data->impact);
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];
        $uker = Management::pluck('title', 'id');
        $category = Category::pluck('title', 'id');
        $possibilities = Possibility::full_title();
        $impacts = Impact::full_title();

        return view('pages.risk.statement.form', compact('page_title', 'page_description', 'data', 'category', 'uker', 'possibilities', 'impacts'));
    }

    public function create_next()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];
        $uker = Management::pluck('title', 'id');
        $category = Category::pluck('title', 'id');
        $possibilities = Possibility::full_title();
        $impacts = Impact::full_title();

        return view('pages.risk.statement.create', compact('page_title', 'page_description', 'data', 'category', 'uker', 'possibilities', 'impacts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), $this->rules($request, false));
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Pernyataan Risiko dan Penyebab tidak boleh kosong');
        }

        $store = new RiskStatement();
        $store->fill($request->all());
        $store->save();

        return redirect(route('statement.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = RiskStatement::find($id);
        $uker = Management::pluck('title', 'id');
        $category = Category::pluck('title', 'id');
        $possibilities = Possibility::full_title();
        $impacts = Impact::full_title();

        return view('pages.risk.statement.form', compact('page_title', 'page_description', 'data', 'category', 'uker', 'possibilities', 'impacts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules($request, false));
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Pernyataan Risiko dan Penyebab tidak boleh kosong');
        }

        $update = RiskStatement::find($id);
        $update->fill($request->all());
        $update->update();
        $uker_id = $update->risk_profile->management->parent->id ?? 1;

        return redirect(route('statement.index', ['uker_id' => $uker_id]))->with('success', show_message('save'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = RiskStatement::find($id);
        $destroy->delete();

        return redirect()->back()->with('success', show_message('delete'));
    }
}
