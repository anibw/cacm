<?php

namespace App\Http\Controllers\Risk;

use App\Http\Controllers\Controller;
use App\Models\Management;
use App\Models\RiskProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Datatables;

class ProfileController extends Controller
{
    private $page_title = 'Tujuan Kegiatan Utama';

    private function rules(Request $request, bool $isEdit)
    {
        $rules = [
            'semester' => 'required|integer',
            'number' => 'required|min:1|max:25',
            'target' => 'required|min:5|max:250',
            'indicator' => 'required|min:5|max:250',
            'activity' => 'required|min:5|max:250',
            'goal' => 'required|min:5|max:250',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $uker_id = 1;
        if (isset($request->uker_id)) {
            $uker_id = $request->uker_id;
        }
        $page_title = $this->page_title;
        $page_description = 'list data -> Periode ' . period()->period;
        $uker = Management::pluck('title', 'id');

        return view('pages.risk.profile.index', compact('page_title', 'page_description', 'uker', 'uker_id'));
    }

    /**
     * Show the list of targets data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $period_id = period()->id;
        $uker_id = $request->uker_id;
        $mng_id = Management::getIncludeID($uker_id);

        $data = RiskProfile::where('period_id', '=', $period_id)->whereIn('management_id', $mng_id)
            ->orderBy('management_id', 'asc')->orderBy('id', 'asc')->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return act_button(['edit', 'delete'], '/risk/profile/', $data->id);
            })
            ->addColumn('uker', function ($data) {
                return $data->management->title;
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];
        $upr = Management::pluck('title', 'id');

        return view('pages.risk.profile.form', compact('page_title', 'page_description', 'data', 'upr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules($request, false));
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data nomor, semester, dan tujuan harus terisi');
        }

        RiskProfile::create($request->all());

        return redirect(route('profile.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = RiskProfile::find($id);
        $upr = Management::pluck('title', 'id');

        return view('pages.risk.profile.form', compact('page_title', 'page_description', 'data', 'upr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules($request, false));
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data nomor, semester, dan tujuan harus terisi');
        }

        $update = RiskProfile::find($id);
        $update->fill($request->all());
        $update->update();
        $uker_id = $update->management->parent->id ?? 1;

        return redirect(route('profile.index', ['uker_id' => $uker_id]))->with('success', show_message('update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = RiskProfile::find($id);
        $destroy->delete();

        return redirect()->back()->with('success', show_message('delete'));
    }
}
