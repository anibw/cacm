<?php

namespace App\Http\Controllers\Risk;

use App\Http\Controllers\Controller;
use App\Models\Impact;
use App\Models\Management;
use App\Models\Possibility;
use App\Models\Response;
use App\Models\RiskStatement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;

class ResponseController extends Controller
{
    private $page_title = 'Respon Risiko';

    private function rules(Request $request, bool $isEdit)
    {
        $rules = [
            'response' => 'required|min:5|max:250',
            'possibility' => 'integer',
            'impact' => 'integer',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $uker_id = 1;
        if (isset($request->uker_id)) {
            $uker_id = $request->uker_id;
        }
        $page_title = $this->page_title;
        $page_description = 'list data -> Periode ' . period()->period;
        $uker = Management::pluck('title', 'id');

        return view('pages.risk.response.index', compact('page_title', 'page_description', 'uker', 'uker_id'));
    }

    /**
     * Show the list of targets data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $risk_profiles = q_risk_profiles($request->uker_id);

        $data = Response::select('responses.id', 'responses.risk_statement_id', 'responses.response', 'responses.innovation', 'responses.resource', 'responses.possibility', 'responses.impact', 'responses.priority')
            ->joinSub($risk_profiles, 'profiles', function ($join) {
                $join->on('profiles.id', '=', 'responses.risk_profile_id');
            })->orderBy('responses.risk_statement_id', 'asc')
            ->get();

        return DataTables::of($data)
            ->editColumn('possibility', function ($data) {
                return lbl_possibility($data->possibility);
            })
            ->editColumn('impact', function ($data) {
                return lbl_impact($data->impact);
            })
            ->addColumn('action', function ($data) {
                return act_button(['edit', 'delete'], '/risk/response/', $data->id);
            })
            ->addColumn('risk_statement', function ($data) {
                return $data->risk_statement->risk_statement;
            })
            ->addColumn('st_possibility', function ($data) {
                return lbl_possibility($data->risk_statement->control->possibility);
            })
            ->addColumn('st_impact', function ($data) {
                return lbl_impact($data->risk_statement->control->impact);
            })
            ->addColumn('st_risk', function ($data) {
                return btn_risk_cluster($data->risk_statement->control->possibility, $data->risk_statement->control->impact);
            })
            ->addColumn('risk_level', function ($data) {
                return btn_risk_cluster($data->possibility, $data->impact);
            })
            // ->addColumn('target', function ($data) {
            //     return innovation_target($data->innovation_target);
            // })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $risk_statement = RiskStatement::whereIn('id', $request->statement_id)->get();

        foreach ($risk_statement as $statement) {
            Response::create([
                'risk_profile_id' => $statement->risk_profile_id,
                'risk_statement_id' => $statement->id,
                'possibility' => $statement->control->possibility,
                'impact' => $statement->control->impact,
                'score' => $statement->control->score,
                'priority' => $statement->control->priority
            ]);
        }

        return redirect(route('response.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = Response::find($id);
        $possibilities = Possibility::full_title();
        $impacts = Impact::full_title();
        $date_now = Carbon::now()->format('Y-m-d');

        return view('pages.risk.response.form', compact('page_title', 'page_description', 'data', 'possibilities', 'impacts', 'date_now'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules($request, false));
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data isian ada yang salah');
        }

        $update = Response::find($id);
        $update->fill($request->all());
        $update->update();
        $uker_id = $update->risk_profile->management->parent->id ?? 1;

        return redirect(route('response.index', ['uker_id' => $uker_id]))->with('success', show_message('save'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Response::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
