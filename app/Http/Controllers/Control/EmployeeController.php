<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\Models\AuditorLevel;
use App\Models\Employee;
use App\Models\Rank;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    private $page_title = 'Manajemen Pegawai';

    private function rules()
    {
        return $rules = [
            'nip' => 'required|min:3|max:15',
            'name' => 'required|min:3|max:150',
            'gol_ruang' => 'required',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data';

        return view('pages.control.employee.index', compact('page_title', 'page_description'));
    }

    /**
     * Show the list of users data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $emp = Employee::all();
        return DataTables::of($emp)
            ->editColumn('gol_ruang', function ($emp) {
                return $emp->rank->gol_ruang;
            })
            ->editColumn('auditor_level_id', function ($emp) {
                return $emp->auditor_level->auditor_level;
            })
            ->addColumn('action', function ($emp) {
                // return act_button(['show', 'edit', 'delete'], '/control/employee/', $emp->id);
                return act_button(['edit', 'delete'], '/control/employee/', $emp->id);
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];
        $gol_ruang = Rank::pluck('gol_ruang', 'kode');
        $auditor_level = AuditorLevel::pluck('auditor_level', 'id');

        return view('pages.control.employee.form', compact('page_title', 'page_description', 'data', 'gol_ruang', 'auditor_level'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data isian tidak sesuai');
        }

        Employee::create($request->all());

        return redirect(route('employee.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = Employee::find($id);
        $gol_ruang = Rank::pluck('gol_ruang', 'kode');
        $auditor_level = AuditorLevel::pluck('auditor_level', 'id')->prepend('-', 0);

        return view('pages.control.employee.form', compact('page_title', 'page_description', 'data', 'gol_ruang', 'auditor_level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data isian tidak sesuai');
        }

        $update = Employee::find($id);
        $update->fill($request->all());
        $update->update();

        return redirect(route('employee.index'))->with('success', show_message('save'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Employee::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
