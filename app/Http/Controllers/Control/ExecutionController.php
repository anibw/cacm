<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\Models\AuditExecution;
use App\Models\AuditPlan;
use App\Models\AuditReport;
use App\Models\AuditType;
use App\Models\SupervisionType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ExecutionController extends Controller
{
    private $page_title = 'Pelaksanaan Audit';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data -> Periode ' . period()->period;

        return view('pages.control.execution.index', compact('page_title', 'page_description'));
    }

    /**
     * Show the list of users data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $period_id = period()->id;
        $data = AuditExecution::where('period_id', '=', $period_id)->get();

        if (isset($request->req_from)) {
            $req_from = $request->req_from;
            if ($req_from == 'report') {
                $audit_execution_id = AuditReport::pluck('audit_execution_id');
                $data = $data->whereNotIn('id', $audit_execution_id);
            }
        }
        return DataTables::of($data)
            ->addColumn('supervision', function ($data) {
                return '0' . $data->supervision_type_id . '-' . $data->supervision_type->supervision_type;
            })
            ->editColumn('status', function ($data) {
                return status_penugasan($data->status);
            })
            ->editColumn('time_plan', function ($data) {
                return get_month()[$data->time_plan];
            })
            ->addColumn('action', function ($data) {
                return act_button(['show', 'edit', 'delete'], '/control/execution/', $data->id);
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];
        $audit_type = AuditType::pluck('audit_type', 'id');
        $supervision = SupervisionType::pluck('supervision_type', 'id');
        $status = get_status_penugasan();
        $time_plan = get_month();

        $date_time = Carbon::now('+7');
        $date_now = $date_time->format('Y-m-d');
        $jenis_st = get_jenis_st();

        return view('pages.control.execution.form', compact('page_title', 'page_description', 'data', 'audit_type', 'supervision', 'status', 'time_plan', 'date_now', 'jenis_st'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->audit_plan_id)) {
            $audit_plans = AuditPlan::whereIn('id', $request->audit_plan_id)->get();

            foreach ($audit_plans as $audit_plan) {
                AuditExecution::create([
                    'period_id' => $audit_plan->period_id,
                    'code' => $audit_plan->code,
                    'title' => $audit_plan->title,
                    'is_pkpt' => $audit_plan->is_pkpt,
                    'status' => $audit_plan->status,
                    'time_plan' => $audit_plan->time_plan,
                    'triwulan' => $audit_plan->triwulan,
                    'audit_type_id' => $audit_plan->audit_type_id,
                    'supervision_type_id' => $audit_plan->supervision_type_id,
                    'audit_plan_id' => $audit_plan->id,
                    // 'management_id' => $audit_plan->management_id,
                ]);
            }
        } else {
            $request['period_id'] = period()->id;
            $request['triwulan'] = get_triwulan($request->time_plan);
            $store = AuditExecution::create($request->all());
            $store->update_code();
        }

        return redirect(route('execution.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page_title = $this->page_title;
        $page_description = 'tampil data';
        $data = AuditExecution::find($id);
        $audit_type = AuditType::pluck('audit_type', 'id');
        $supervision = SupervisionType::pluck('supervision_type', 'id');
        $status = get_status_penugasan();
        $time_plan = get_month();

        $date_time = Carbon::now('+7');
        $date_now = $date_time->format('Y-m-d');
        $tanggal_st = is_null($data->tanggal_st) ? $date_now : Carbon::parse($data->tanggal_st)->format('Y-m-d');
        $tmt_awal = is_null($data->tmt_awal) ? $date_now : Carbon::parse($data->tmt_awal)->format('Y-m-d');
        $tmt_akhir = is_null($data->tmt_akhir) ? $date_now : Carbon::parse($data->tmt_akhir)->format('Y-m-d');
        $jenis_st = get_jenis_st();

        return view('pages.control.execution.show', compact('page_title', 'page_description', 'data', 'audit_type', 'supervision', 'status', 'time_plan', 'tanggal_st', 'tmt_awal', 'tmt_akhir', 'jenis_st'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = AuditExecution::find($id);
        $audit_type = AuditType::pluck('audit_type', 'id');
        $supervision = SupervisionType::pluck('supervision_type', 'id');
        $status = get_status_penugasan();
        $time_plan = get_month();

        $date_time = Carbon::now('+7');
        $date_now = $date_time->format('Y-m-d');
        $tanggal_st = is_null($data->tanggal_st) ? $date_now : Carbon::parse($data->tanggal_st)->format('Y-m-d');
        $tmt_awal = is_null($data->tmt_awal) ? $date_now : Carbon::parse($data->tmt_awal)->format('Y-m-d');
        $tmt_akhir = is_null($data->tmt_akhir) ? $date_now : Carbon::parse($data->tmt_akhir)->format('Y-m-d');
        $jenis_st = get_jenis_st();

        return view('pages.control.execution.form', compact('page_title', 'page_description', 'data', 'audit_type', 'supervision', 'status', 'time_plan', 'tanggal_st', 'tmt_awal', 'tmt_akhir', 'jenis_st'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = AuditExecution::find($id);
        $request['triwulan'] = get_triwulan($request->time_plan);
        $update->fill($request->all());
        $update->update();

        $update->update_code();

        return redirect(route('execution.index'))->with('success', show_message('save'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = AuditExecution::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
