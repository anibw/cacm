<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\Models\AuditExecution;
use App\Models\AuditReport;
use App\Models\SupervisionType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ReportController extends Controller
{
    private $page_title = 'Pelaporan Audit';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data -> Periode ' . period()->period;

        return view('pages.control.report.index', compact('page_title', 'page_description'));
    }

    /**
     * Show the list of users data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $data = AuditReport::all();

        return DataTables::of($data)
            ->addColumn('supervision', function ($data) {
                return '0' . $data->supervision_type_id . '-' . $data->supervision_type->supervision_type;
            })
            ->editColumn('status', function ($data) {
                return status_penugasan($data->status);
            })
            ->addColumn('action', function ($data) {
                return act_button(['edit', 'delete'], '/control/report/', $data->id);
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $audit_datas = AuditExecution::whereIn('id', $request->audit_execution_id)->get();

        foreach ($audit_datas as $audit_data) {
            AuditReport::create([
                'code' => $audit_data->code,
                'title' => $audit_data->title,
                'report_date' => Carbon::now('+7')->format('Y-m-d'),
                'status' => 0,
                'audit_execution_id' => $audit_data->id,
                'supervision_type_id' => $audit_data->supervision_type_id,
                // 'management_id' => $audit_plan->management_id,
            ]);
        }
        return redirect(route('report.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = AuditReport::find($id);
        // $supervision = SupervisionType::pluck('supervision_type', 'id');
        $status = get_status_penugasan();

        $date_time = Carbon::now('+7');
        $date_now = $date_time->format('Y-m-d');
        $report_date = is_null($data->report_date) ? $date_now : Carbon::parse($data->report_date)->format('Y-m-d');

        return view('pages.control.report.form', compact('page_title', 'page_description', 'data', 'status', 'report_date'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = AuditReport::find($id);
        $update->fill($request->all());
        $update->update();

        return redirect(route('report.index'))->with('success', show_message('save'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = AuditReport::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
