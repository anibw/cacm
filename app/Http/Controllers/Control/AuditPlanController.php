<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\Models\AuditExecution;
use App\Models\AuditPlan;
use App\Models\AuditType;
use App\Models\RiskStatement;
use App\Models\SupervisionType;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AuditPlanController extends Controller
{
    private $page_title = 'Perencanaan Pengawasan';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data -> Periode ' . period()->period;

        return view('pages.control.planning.index', compact('page_title', 'page_description'));
    }

    /**
     * Show the list of users data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $period_id = period()->id;
        $data = AuditPlan::where('period_id', '=', $period_id)->get();

        if (isset($request->req_from)) {
            $req_from = $request->req_from;
            if ($req_from == 'execution') {
                $audit_plan_id = AuditExecution::pluck('audit_plan_id');
                $data = $data->whereNotIn('id', $audit_plan_id);
            }
        }

        return DataTables::of($data)
            ->addColumn('supervision', function ($data) {
                return '0' . $data->supervision_type_id . '-' . $data->supervision_type->supervision_type;
            })
            ->editColumn('status', function ($data) {
                return status_penugasan($data->status);
            })
            ->editColumn('time_plan', function ($data) {
                return get_month()[$data->time_plan];
            })
            ->addColumn('action', function ($data) {
                return act_button(['edit', 'delete'], '/control/planning/', $data->id);
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];
        $audit_type = AuditType::pluck('audit_type', 'id');
        $supervision = SupervisionType::pluck('supervision_type', 'id');
        $status = get_status_penugasan();
        $time_plan = get_month();

        return view('pages.control.planning.form', compact('page_title', 'page_description', 'data', 'audit_type', 'supervision', 'status', 'time_plan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->statement_id)) {
            $risk_statements = RiskStatement::whereIn('id', $request->statement_id)->get();

            foreach ($risk_statements as $risk_statement) {
                $new_data = new AuditPlan();
                // $new_data = AuditPlan::create([
                $new_data->period_id = period()->id;
                $new_data->title = $risk_statement->risk_statement;
                $new_data->is_pkpt = 1;
                $new_data->status = 0;
                $new_data->time_plan = 1;
                $new_data->triwulan = 1;
                $new_data->audit_type_id = 1;
                $new_data->supervision_type_id = 1;
                $new_data->risk_statement_id = $risk_statement->id;
                // 'management_id' => $audit_plan->management_id,
                // ]);
                $new_data->save();
                $new_data->update_code();
            }
        } else {
            $request['period_id'] = period()->id;
            $request['triwulan'] = get_triwulan($request->time_plan);
            $store = AuditPlan::create($request->all());
            $store->update_code();
        }

        return redirect(route('planning.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = AuditPlan::find($id);
        $audit_type = AuditType::pluck('audit_type', 'id');
        $supervision = SupervisionType::pluck('supervision_type', 'id');
        $status = get_status_penugasan();
        $time_plan = get_month();

        return view('pages.control.planning.form', compact('page_title', 'page_description', 'data', 'audit_type', 'supervision', 'status', 'time_plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = AuditPlan::find($id);
        $request['period_id'] = period()->id;
        $request['triwulan'] = get_triwulan($request->time_plan);
        $update->fill($request->all());
        $update->update();

        $update->update_code();

        return redirect(route('planning.index'))->with('success', show_message('save'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = AuditPlan::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
