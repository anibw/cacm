<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Management;
use App\Models\Stakeholder;
use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;

class StakeholderController extends Controller
{
    private $page_title = 'Pemangku Kepentingan';

    private function rules(Request $request, bool $isEdit)
    {
        $rules = [
            'activity' => 'required|min:5|max:250',
            'goal' => 'required|min:5|max:250',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data -> Periode ' . period()->period;
        $uker = Management::pluck('title', 'id');

        return view('pages.setting.stakeholder.index', compact('page_title', 'page_description', 'uker'));
    }

    /**
     * Show the list of targets data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $uker_id = $request->uker_id;
        $mng_id = Management::getIncludeID($uker_id);

        $data = Stakeholder::whereIn('management_id', $mng_id)
            ->orderBy('nomor', 'asc')->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return act_button(['edit', 'delete'], '/setting/stakeholder/', $data->id);
            })
            ->addColumn('unsur', function ($data) {
                if ($data->status == 1) {
                    return 'Internal';
                } else {
                    return 'Eksternal';
                }
            })
            ->addColumn('uker', function ($data) {
                return $data->management->title;
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $uker = Management::find($request->uker_id);
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];

        return view('pages.setting.stakeholder.form', compact('page_title', 'page_description', 'data', 'uker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validator = Validator::make($request->all(), $this->rules());
        // if ($validator->fails()) {
        //     return redirect()->back()->withInput()->with('error', 'Data isian tidak sesuai');
        // }

        Stakeholder::create($request->all());

        return redirect(route('stakeholder.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = Stakeholder::find($id);
        $uker = Management::find($data->management_id);

        return view('pages.setting.stakeholder.form', compact('page_title', 'page_description', 'data', 'uker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Stakeholder::find($id);
        $update->fill($request->all());
        $update->update();

        return redirect(route('stakeholder.index'))->with('success', show_message('update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Stakeholder::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
