<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $page_title = 'Setting User';
    private $user_type = [1 => 'Pemilik Risiko', 2 => 'Pengelola Risiko'];

    private function rules(Request $request, bool $isEdit)
    {
        $rules = [];
        if ($isEdit) {
            $rules = [
                'name' => 'required|min:3|max:150',
                // 'password' => 'min:8|required_with:password_confirmation|same:password_confirmation',
                // 'password_confirmation' => 'min:8'
            ];
        } else {
            $rules = [
                'name' => 'required|min:3|max:150',
                'email' => 'required|unique:users,email',
                'password' => 'min:8|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'min:8'
            ];
        }

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data';

        return view('pages.setting.user.index', compact('page_title', 'page_description'));
    }

    /**
     * Show the list of users data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $user = User::all();
        return Datatables::of($user)
            ->addColumn('tipe', function ($user) {
                if ($user->user_type == 0) {
                    return 'Administrator';
                } else {
                    return $this->user_type[$user->user_type];
                }
            })
            ->addColumn('action', function ($user) {
                if ($user->user_type == 0) {
                    return act_button(['edit'], '/setting/user/', $user->id);
                } else {
                    return act_button(['edit', 'delete'], '/setting/user/', $user->id);
                }
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data user';
        $data = [];
        $user_type = $this->user_type;

        return view('pages.setting.user.form', compact('page_title', 'page_description', 'data', 'user_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules($request, false));
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data email yang sama, Password konfirmasi tidak sama');
        }

        $store = new User;
        $store->name = $request->name;
        $store->email = $request->email;
        $store->user_type = $request->user_type;
        $store->email_verified_at = now();
        $store->password = bcrypt($request->password);
        $store->save();

        return redirect(route('user.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data user';
        $data = User::find($id);
        $user_type = $this->user_type;

        return view('pages.setting.user.form', compact('page_title', 'page_description', 'data', 'user_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules($request, true));
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data email yang sama, Password konfirmasi tidak sama');
        }

        $data = User::find($id);
        $data->name = $request->name;
        $data->user_type = $request->user_type;
        if (!empty($request->password)) {
            $data->password = bcrypt($request->password);
        }
        $data->update();

        return redirect(route('user.index'))->with('success', show_message('update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = User::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
