<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Impact;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class ImpactController extends Controller
{
    private $page_title = 'Level Dampak';

    private function rules()
    {
        $rules = [
            'level' => 'required|numeric',
            'title' => 'max:100',
            'description' => 'max:250',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data';

        return view('pages.setting.impact.index', compact('page_title', 'page_description'));
    }

    /**
     * Show the list of data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list()
    {
        $data = Impact::all();
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return act_button(['edit'], '/setting/impact/', $data->id);
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = Impact::find($id);

        return view('pages.setting.impact.form', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data isian tidak sesuai');
        }

        $update = Impact::find($id);
        $update->fill($request->all());
        $update->update();

        return redirect(route('impact.index'))->with('success', show_message('update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
