<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    private $page_title = 'Kategori Risiko';

    private function rules()
    {
        $rules = [
            'title' => 'max:100',
            'description' => 'max:250',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data';

        return view('pages.setting.category.index', compact('page_title', 'page_description'));
    }

    /**
     * Show the list of data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list()
    {
        $data = Category::all();
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return act_button(['edit', 'delete'], '/setting/category/', $data->id);
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];

        return view('pages.setting.category.form', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data isian tidak sesuai');
        }

        Category::create($request->all());

        return redirect(route('category.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = Category::find($id);

        return view('pages.setting.category.form', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data isian tidak sesuai');
        }

        $update = Category::find($id);
        $update->fill($request->all());
        $update->update();

        return redirect(route('category.index'))->with('success', show_message('update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Category::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
