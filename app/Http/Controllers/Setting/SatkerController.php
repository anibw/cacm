<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Management;
use Illuminate\Http\Request;

class SatkerController extends Controller
{
    private $page_title = 'Satuan Kerja';

    private function rules()
    {
        $rules = [
            'title' => 'required|min:3|max:150',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data';

        return view('pages.setting.management.index', compact('page_title', 'page_description'));
    }

    /**
     * Get all menu data in ajax.
     *
     * @return \App\Region to Json
     */
    public function list_tree()
    {
        return collect(Management::item_tree())->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Management::create($request->all());

        return redirect(route('satker.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $update = Management::find($id);
        $update->fill($request->all());
        $update->parent_id = $request->parent_id == '#' ? null : $request->parent_id;
        $update->update();

        return redirect(route('satker.index'))->with('success', show_message('update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Management::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
