<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Impact;
use App\Models\RiskImpact;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class RiskImpactController extends Controller
{
    private $page_title = 'Dampak Risiko';

    private function rules()
    {
        $rules = [
            'description' => 'max:250',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category_id = 1;
        if (isset($request->category_id)) {
            $category_id = $request->category_id;
        }
        $page_title = $this->page_title;
        $page_description = 'list data';
        $category = Category::pluck('title', 'id');

        return view('pages.setting.risk_impact.index', compact('page_title', 'page_description', 'category', 'category_id'));
    }

    /**
     * Show the list of data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list(Request $request)
    {
        $category_id = $request->category_id;

        $level0 = get_risk_impact($category_id, 0);
        $level1 = get_risk_impact($category_id, 1);
        $level2 = get_risk_impact($category_id, 2);
        $data = DB::table('risk_impacts AS risk')
            ->select('risk.category_id', 'risk.impact_id AS level', 'impacts.title', 'level0.description AS level_0', 'level1.description AS level_1', 'level2.description AS level_2')
            ->join('impacts', 'impacts.id', '=', 'risk.impact_id')
            ->joinSub($level0, 'level0', function ($join) {
                $join->on('level0.impact_id', '=', 'risk.impact_id');
            })
            ->joinSub($level1, 'level1', function ($join) {
                $join->on('level1.impact_id', '=', 'risk.impact_id');
            })
            ->joinSub($level2, 'level2', function ($join) {
                $join->on('level2.impact_id', '=', 'risk.impact_id');
            })
            ->where('risk.category_id', '=', $category_id)
            ->groupBy('risk.impact_id', 'impacts.title', 'risk.category_id')
            ->get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return act_button(['edit'], '/setting/risk_impact/', $data->category_id . '-' . $data->level);
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        [$category_id, $impact_id] = explode('-', $id);
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = RiskImpact::where('category_id', '=', $category_id)
            ->where('impact_id', '=', $impact_id)->get();
        $category = Category::find($category_id);
        $impact = Impact::find($impact_id);

        return view('pages.setting.risk_impact.form', compact('page_title', 'page_description', 'data', 'category', 'impact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        [$category_id, $impact_id] = explode('-', $id);
        $mng_level = $request->mng_level;
        foreach ($mng_level as $key => $value) {
            RiskImpact::where('category_id', $category_id)
                ->where('impact_id', $impact_id)
                ->where('mng_level', $key)
                ->update(['description' => $value]);
        }

        return redirect(route('risk_impact.index', ['category_id' => $category_id]))->with('success', show_message('update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
