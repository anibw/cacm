<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Cluster;
use App\Models\Score;

class RiskmapController extends Controller
{
    private $page_title = 'Peta Risiko';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = '';
        $chart_risk = Score::heat_series('priority');
        $chart_risk['title'] .= ' berdasar Prioritas';

        return view('pages.setting.riskmap.index', compact('page_title', 'page_description', 'chart_risk'));
    }
}
