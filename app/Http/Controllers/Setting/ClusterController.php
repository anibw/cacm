<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Cluster;
use App\Models\Score;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;

class ClusterController extends Controller
{
    /**
     * Show the list of data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list()
    {
        $data = Cluster::all();
        return Datatables::of($data)
            ->addColumn('risk_level', function ($data) {
                $title = preg_replace('/(?<=\w)./', '', $data->name);
                $title = '<button type="button" class="btn btn-sm text-white font-weight-bold" data-toggle="tooltip" data-placement="top" style="background-color:' . $data->color . '">' . $title . '</button>';
                return Str::of($title)->toHtmlString();
            })
            ->addColumn('title', function ($data) {
                $title = '<button type="button" class="btn btn-sm text-white font-weight-bold" data-toggle="tooltip" data-placement="top" style="background-color:' . $data->color . '">' . $data->name . '</button>';
                return Str::of($title)->toHtmlString();
            })
            ->addColumn('action', function ($data) {
                $btn_edit = '<button class="btn btn-icon btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Data" href="#" onclick="cluster_edit(' . $data->id . ')"><i class="fas fa-pencil-alt"></i></button>';
                return $btn_edit;
            })
            ->make(true);
    }

    public function update(Request $request, $id)
    {
        $update = Cluster::find($id);
        $update->name = $request->modal_name;
        $update->from = $request->modal_from;
        $update->to = $request->modal_to;
        $update->color = $request->modal_color;
        $update->update();

        Score::whereBetween('priority', [$request->modal_from, $request->modal_to])
            ->update(['cluster_id' => $id]);

        return redirect(route('riskmap.index'))->with('success', show_message('update'));
    }

    public function get_item(Request $request)
    {
        $score = risk_cluster($request->possibility_id, $request->impact_id);
        $data = $score->cluster->toArray();
        $data['priority'] = $score->priority;
        return response()->json($data);
    }
}
