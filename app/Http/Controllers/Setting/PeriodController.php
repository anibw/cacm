<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Period;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PeriodController extends Controller
{
    private $page_title = 'Periode Anggaran';

    private function rules()
    {
        $rules = [
            'period' => 'required|numeric',
            'description' => 'required|min:3|max:250',
        ];

        return $rules;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_title = $this->page_title;
        $page_description = 'list data';

        return view('pages.setting.period.index', compact('page_title', 'page_description'));
    }

    public function set_active(Request $request)
    {
        Period::query()->update(['active' => 0]);
        $period = Period::find($request->data_id);
        $period->active = 1;
        $period->update();

        return response()->json('OK');
    }

    /**
     * Show the list of data.
     *
     * @return Yajra\Datatables\Datatables
     */
    public function list()
    {
        $data = Period::all();
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                if ($data->active == 1) {
                    return act_button(['edit'], '/setting/period/', $data->id);
                } else {
                    return act_button(['edit', 'delete'], '/setting/period/', $data->id);
                }
            })
            ->addColumn('aktif', function ($data) {
                $str_aktif = on_checked($data->id, $data->active == 1);
                return Str::of($str_aktif)->toHtmlString();
            })
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = $this->page_title;
        $page_description = 'tambah data';
        $data = [];

        return view('pages.setting.period.form', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data isian tidak sesuai');
        }

        Period::create($request->all());

        return redirect(route('period.index'))->with('success', show_message('save'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = $this->page_title;
        $page_description = 'edit data';
        $data = Period::find($id);

        return view('pages.setting.period.form', compact('page_title', 'page_description', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', 'Data isian tidak sesuai');
        }

        $update = Period::find($id);
        $update->fill($request->all());
        $update->update();

        return redirect(route('period.index'))->with('success', show_message('update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Period::find($id);
        $destroy->delete();
        return redirect()->back()->with('success', show_message('delete'));
    }
}
