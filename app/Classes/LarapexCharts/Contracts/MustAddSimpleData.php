<?php

namespace App\Classes\LarapexCharts\Contracts;


interface MustAddSimpleData
{
    public function addData(array $data);
}