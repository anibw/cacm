<?php


namespace App\Classes\LarapexCharts\Contracts;


interface MustAddComplexData
{
    public function addData(string $name, array $data);
}