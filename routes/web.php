<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['reset' => false, 'verify' => false]);

Route::get('/', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    // group menu resiko
    Route::prefix('risk')->group(function () {
        Route::post('/profile/list', 'Risk\ProfileController@list')->name('profile.list');
        Route::resource('profile', Risk\ProfileController::class);

        Route::post('/statement/tmp_list', 'Risk\StatementController@tmp_list')->name('statement.tmp_list');
        Route::post('/statement/list', 'Risk\StatementController@list')->name('statement.list');
        Route::resource('statement', Risk\StatementController::class);

        Route::post('/control/list', 'Risk\ControlController@list')->name('control.list');
        Route::resource('control', Risk\ControlController::class);

        Route::post('/response/list', 'Risk\ResponseController@list')->name('response.list');
        Route::resource('response', Risk\ResponseController::class);
    });

    // group menu kendali mutu
    Route::prefix('control')->group(function () {
        Route::post('/employee/list', 'Control\EmployeeController@list')->name('employee.list');
        Route::resource('employee', Control\EmployeeController::class);

        Route::post('/auditan/list', 'Control\AuditanController@list')->name('auditan.list');
        Route::resource('auditan', Control\AuditanController::class);

        Route::post('/planning/list', 'Control\AuditPlanController@list')->name('planning.list');
        Route::resource('planning', Control\AuditPlanController::class);

        Route::post('/execution/list', 'Control\ExecutionController@list')->name('execution.list');
        Route::resource('execution', Control\ExecutionController::class);

        Route::post('/assigment/list', 'Control\AssigmentController@list')->name('assigment.list');
        Route::resource('assigment', Control\AssigmentController::class);

        Route::post('/report/list', 'Control\ReportController@list')->name('report.list');
        Route::resource('report', Control\ReportController::class);
    });

    // group menu setting
    Route::prefix('setting')->group(function () {
        Route::post('/period/set_active', 'Setting\PeriodController@set_active');
        Route::post('/period/list', 'Setting\PeriodController@list')->name('period.list');
        Route::resource('period', Setting\PeriodController::class);

        Route::post('/possibility/list', 'Setting\PossibilityController@list')->name('possibility.list');
        Route::resource('possibility', Setting\PossibilityController::class);

        Route::post('/impact/list', 'Setting\ImpactController@list')->name('impact.list');
        Route::resource('impact', Setting\ImpactController::class);

        Route::post('/category/list', 'Setting\CategoryController@list')->name('category.list');
        Route::resource('category', Setting\CategoryController::class);

        Route::post('/risk_impact/list', 'Setting\RiskImpactController@list')->name('risk_impact.list');
        Route::resource('risk_impact', Setting\RiskImpactController::class);

        Route::get('riskmap', 'Setting\RiskmapController@index')->name('riskmap.index');

        Route::get('/satker/list_tree', 'Setting\SatkerController@list_tree')->name('satker.list_tree');
        Route::resource('satker', Setting\SatkerController::class);

        Route::post('/stakeholder/list', 'Setting\StakeholderController@list')->name('stakeholder.list');
        Route::resource('stakeholder', Setting\StakeholderController::class);

        Route::post('/cluster/item', 'Setting\ClusterController@get_item');
        Route::post('/cluster/list', 'Setting\ClusterController@list')->name('cluster.list');
        Route::put('/cluster/{cluster}', 'Setting\ClusterController@update');

        Route::post('/user/list', 'Setting\UserController@list')->name('user.list');
        Route::resource('user', Setting\UserController::class);
    });
});

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');
