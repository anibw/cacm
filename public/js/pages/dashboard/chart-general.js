"use strict";

const chart_colors = ["#008FFB", "#00E396", "#feb019", "#ff455f", "#775dd0", "#80effe", "#0077B5", "#ff6384", "#c9cbcf", "#0057ff", "#00a9f4", "#2ccdc9", "#5e72e4"];

// Fungsi untuk menampilkan chart type pie
window.pie_chart = function (data) {
    var options = {
        chart: {
            type: 'pie',
            height: 400,
        },
        series: data.series,
        colors: chart_colors,
        labels: data.labels,
        legend: {
            position: 'bottom',
            offsetY: 0,
        },
        xaxis: {
            type: 'category'
        }
    }

    var chart = new ApexCharts(document.getElementById(data.name), options);
    chart.render();
    return chart;
}

// Fungsi untuk menampilkan chart type heat map
window.heat_chart = function (data) {
    var options = {
        series: data.series,
        chart: {
            height: 350,
            type: 'heatmap',
            toolbar: {
                show: false
            }
        },
        plotOptions: {
            heatmap: {
                enableShades: false,
                colorScale: {
                    ranges: data.ranges
                }
            }
        },
        dataLabels: {
            enabled: true,
            formatter: function (value, { seriesIndex, dataPointIndex, w }) {
                return w.config.series[seriesIndex].data[dataPointIndex].score;
            }
        },
        tooltip: {
            enabled: false,
        },
        stroke: {
            width: 1
        },
        xaxis: {
            type: 'category',
            title: {
                text: 'Dampak',
                style: {
                    fontSize: '20px',
                }
            }
        },
        yaxis: {
            title: {
                text: 'Kemungkinan',
                style: {
                    fontSize: '20px',
                }
            }
        },
    };

    var chart = new ApexCharts(document.getElementById(data.name), options);
    chart.render();
    return chart;
}
