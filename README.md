# SIM CACM with Laravel framework

### Introduction

This application use for Risk Management in goverment agency

### Installation

Laravel has a set of requirements in order to ron smoothly in specific environment. Please see [requirements](https://laravel.com/docs/8.x/deployment#server-requirements) section in Laravel documentation.

Javascript similarly uses additional plugins and frameworks, so ensure You have [Composer](https://getcomposer.org/) and [Node](https://nodejs.org/) installed on Your machine.

Assuming your machine meets all requirements - let's process to installation of SIM CACM Laravel integration.

1. Open in cmd or terminal app and navigate to this folder
2. Run following commands

```bash
composer install
```

```bash
cp .env.example .env
```

```bash
php artisan key:generate
```

3. Open .env file and edit your DB configuration

```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=cacm
DB_USERNAME=[username_database]
DB_PASSWORD=[password_database ]
```

```bash
php artisan migrate:fresh --seed
```

4. Application generate user for initial
   User => password : admin@admin.com => admin123; demo@demo.com => demo1234 

```bash
php artisan storage:link
```

```bash
php artisan serve
```

And navigate to generated server link (http://127.0.0.1:8000)

### Copyright

(c)2022
