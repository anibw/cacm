<?php
// Apolication menu initial
return [
    // Dashboard
    [
        'title' => 'Dashboard',
        'root' => true,
        'icon' => 'media/svg/icons/Design/Layers.svg',
        'page' => '/',
        'new-tab' => false,
    ],

    // Custom
    [
        'section' => 'Manajemen Risiko',
    ],
    [
        'title' => 'Manajemen Risiko',
        'icon' => 'media/svg/icons/Layout/Layout-left-panel-2.svg',
        'bullet' => 'dot',
        'root' => true,
        'submenu' => [
            [
                'title' => 'Program dan Kegiatan',
                'page' => 'risk/profile'
            ],
            [
                'title' => 'Pernyataan Risiko',
                'page' => 'risk/statement'
            ],
            [
                'title' => 'Pengendalian',
                'page' => 'risk/control'
            ],
            [
                'title' => 'Respon Risiko',
                'page' => 'risk/response'
            ],
        ]
    ],
    [
        'title' => 'Manajemen Mutu',
        'icon' => 'media/svg/icons/Code/CMD.svg',
        'bullet' => 'dot',
        'root' => true,
        'submenu' => [
            [
                'title' => 'Manajemen Klien',
                'page' => 'control/auditan'
            ],
            [
                'title' => 'Manajemen PKPT',
                'page' => 'control/pkpt'
            ],
            [
                'title' => 'Perencanaan Pengawasan',
                'page' => 'control/planning'
            ],
            [
                'title' => 'Pelaksanaan Pengawasan',
                'page' => 'control/execution'
            ],
            // [
            //     'title' => 'Penugasan Audit',
            //     'page' => 'control/assigment'
            // ],
            [
                'title' => 'Pelaporan Pengawasan',
                'page' => 'control/report'
            ],
            [
                'title' => 'SDM Pengawasan',
                'page' => 'control/employee'
            ],
        ]
    ],

    // Setting
    [
        'section' => 'Konfigurasi',
    ],
    [
        'title' => 'Konfigurasi',
        'desc' => '',
        'icon' => 'media/svg/icons/General/Settings-2.svg',
        'bullet' => 'dot',
        'root' => true,
        'submenu' => [
            [
                'title' => 'Periode',
                'page' => 'setting/period'
            ],
            [
                'title' => 'Manajemen',
                'submenu' => [
                    [
                        'title' => 'Unit Organisasi',
                        'page' => 'setting/satker',
                    ],
                    [
                        'title' => 'Pemangku Kepentingan',
                        'page' => 'setting/stakeholder'
                    ],
                ]
            ],
            [
                'title' => 'Level Kemungkinan',
                'page' => 'setting/possibility'
            ],
            [
                'title' => 'Dampak Risiko',
                'submenu' => [
                    [
                        'title' => 'Level Dampak',
                        'page' => 'setting/impact'
                    ],
                    [
                        'title' => 'Kategori',
                        'page' => 'setting/category'
                    ],
                    [
                        'title' => 'Dampak Risiko',
                        'page' => 'setting/risk_impact'
                    ],
                ]
            ],
            [
                'title' => 'Peta Risiko',
                'page' => 'setting/riskmap'
            ],
            [
                'title' => 'Pengguna',
                'page' => 'setting/user'
            ],
        ]
    ],
];
