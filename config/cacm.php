<?php

return [
    // data management LKPP
    'lkpp' => [
        [
            'title' => 'Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah',
            'sub_mng' => [
                [
                    'title' => 'Sekretariat Utama',
                    'sub_mng' => [
                        [
                            'title' => 'Biro Perencanaan dan Keuangan',
                        ],
                        [
                            'title' => 'Biro Hukum Organisasi dan SDM',
                        ],
                        [
                            'title' => 'Biro Humas Sistem Informasi dan Umum',
                        ],
                    ],
                ],
                [
                    'title' => 'Inspektorat',
                ],
                [
                    'title' => 'Kedeputian Bidang Pengembangan Strategi dan Kebijakan',
                    'sub_mng' => [
                        [
                            'title' => 'Direktorat Pengembangan Strategi dan Kebijakan Pengadaan Umum',
                        ],
                        [
                            'title' => 'Direktorat Pengembangan Strategi dan Kebijakan Pengadaan Khusus',
                        ],
                        [
                            'title' => 'Direktorat Pengembangan Iklim Usaha dan Kerjasama Internasional',
                        ],
                    ],
                ],
                [
                    'title' => 'Kedeputian Bidang Monitoring Evaluasi dan Pengembangan Sistem Informasi',
                    'sub_mng' => [
                        [
                            'title' => 'Direktorat Perencanaan Monitoring dan Evaluasi Pengadaan',
                        ],
                        [
                            'title' => 'Direktorat Pengembangan Sistem Katalog',
                        ],
                        [
                            'title' => 'Direktorat Pengembangan Sistem Pengadaan Secara Elektronik',
                        ],
                    ],
                ],
                [
                    'title' => 'Kedeputian Bidang Pengembangan dan Pembinaan Sumber Daya Manusia',
                    'sub_mng' => [
                        [
                            'title' => 'Direktorat Pengembangan Profesi dan Kelembagaan',
                        ],
                        [
                            'title' => 'Direktorat Sertifikasi Profesi',
                        ],
                    ],
                ],
                [
                    'title' => 'Kedeputian Bidang Hukum dan Penyelesaian Sanggah',
                    'sub_mng' => [
                        [
                            'title' => 'Direktorat Advokasi Pemerintah Pusat',
                        ],
                        [
                            'title' => 'Direktorat Advokasi Pemerintah Daerah',
                        ],
                        [
                            'title' => 'Direktorat Penanganan Permasalahan Hukum',
                        ],
                    ],
                ],
            ],
        ],
    ],

    // data possibility
    'possibility' => [
        [
            'level'         => 1,
            'title'         => 'Hampir Tidak',
            'tolerance'     => '≤ 1 kejadian dalam lebih dari 5 tahun terakhir',
            'percentage'    => 'x ≤ 1%',
            'frequency'     => 'x < 2 kali dalam 1 tahun'
        ],
        [
            'level'         => 2,
            'title'         => 'Jarang',
            'tolerance'     => '1 kejadian dalam lebih dari 5 tahun terakhir',
            'percentage'    => '1% < x ≤ 10%',
            'frequency'     => '2 < x ≤ 5 kali dalam 1 tahun'
        ],
        [
            'level'         => 3,
            'title'         => 'Kadang',
            'tolerance'     => '1 kejadian dalam lebih dari 3 tahun terakhir',
            'percentage'    => '10% < x ≤ 20%',
            'frequency'     => '6 < x ≤ 9 kali dalam 1 tahun'
        ],
        [
            'level'         => 4,
            'title'         => 'Sering',
            'tolerance'     => '1 kejadian dalam lebih dari 2 tahun terakhir',
            'percentage'    => '20% < x ≤ 50%',
            'frequency'     => '10 < x ≤ 12 kali dalam 1 tahun'
        ],
        [
            'level'         => 5,
            'title'         => 'Hampir Pasti',
            'tolerance'     => '≥ 1 kejadian dalam lebih dari 1 tahun terakhir',
            'percentage'    => 'x > 50%',
            'frequency'     => 'x > 12 kali dalam 1 tahun'
        ],
    ],

    // category impact
    'category' => [
        [
            'title' => 'Risiko Keuangan',
            'description' => '',
        ],
        [
            'title' => 'Risiko Reputasi',
            'description' => '',
        ],
        [
            'title' => 'Risiko Hukum',
            'description' => '',
        ],
        [
            'title' => 'Kecelakaan dan Penyakit',
            'description' => '',
        ],
        [
            'title' => 'Risiko Layanan',
            'description' => '',
        ],
        [
            'title' => 'Risiko Kinerja',
            'description' => '',
        ],
        // [
        //     'title' => 'Risiko Fraud/Kecurangan',
        //     'description' => '',
        // ],
    ],

    // data impact
    'impact' => [
        [
            'level'         => 1,
            'title'         => 'Tidak Siginifikan',
            'description'   => 'Dampak yang timbul tidak siginifikan',
        ],
        [
            'level'         => 2,
            'title'         => 'Minor',
            'description'   => 'Dampak yang timbul minor',
        ],
        [
            'level'         => 3,
            'title'         => 'Moderat',
            'description'   => 'Dampak yang timbul moderat',
        ],
        [
            'level'         => 4,
            'title'         => 'Signifikan',
            'description'   => 'Dampak yang timbul cukup signifikan',
        ],
        [
            'level'         => 5,
            'title'         => 'Sangat Signifikan',
            'description'   => 'Dampak yang timbul sangat siginifikan',
        ],
    ],

    // risk impacts
    'risk_impact' => [
        [
            'category_id' => 1,
            'impacts' => [
                [
                    'impact_id' => 1,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => 'x ≤ 0.1% nilai penerimaan/pembiayaan yang dikelola, atau x ≤ 0.05% nilai belanja/aset/kegiatan lain yang dikelola'
                        ],
                    ],
                ],
                [
                    'impact_id' => 2,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => '0.1% < x ≤ 0.5% nilai penerimaan/pembiayaan yang dikelola, atau 0.05% < x ≤ 0.25% nilai belanja/aset/kegiatan lain yang dikelola'
                        ],
                    ],
                ],
                [
                    'impact_id' => 3,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => '0.5% < x ≤ 1% nilai penerimaan/pembiayaan yang dikelola, atau 0.25% < x ≤ 0.5% nilai belanja/aset/kegiatan lain yang dikelola'
                        ],
                    ],
                ],
                [
                    'impact_id' => 4,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => '1% < x ≤ 2% nilai penerimaan/pembiayaan yang dikelola, atau 0.5% < x ≤ 1% nilai belanja/aset/kegiatan lain yang dikelola'
                        ],
                    ],
                ],
                [
                    'impact_id' => 5,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => 'x > 2% nilai penerimaan/pembiayaan yang dikelola, atau x > 1% nilai belanja/aset/kegiatan lain yang dikelola'
                        ],
                    ],
                ],
            ]
        ],
        [
            'category_id' => 2,
            'impacts' => [
                [
                    'impact_id' => 1,
                    'mng_level' => [
                        [
                            'level' => [0, 1],
                            'description' => 'Jumlah keluhan secara lisan (dapat didokumentasikan)/ tertulis ke Lembaga/Unit Organisasi ≤ 10. Tingkat kepercayaan stakeholder sangat baik. Tingkat kepuasan pengguna layanan 4.25 ≤ x ≤ 5 (skala 5)'
                        ],
                        [
                            'level' => [2],
                            'description' => 'Jumlah keluhan secara lisan (dapat didokumentasikan)/tertulis ke Lembaga/Unit Organisasi ≤ 3. Tingkat kepuasan pengguna layanan 4.25 ≤ x ≤ 5 (skala 5)'
                        ],
                    ],
                ],
                [
                    'impact_id' => 2,
                    'mng_level' => [
                        [
                            'level' => [0, 1],
                            'description' => 'Jumlah keluhan secara lisan (dapat didokumentasikan)/ tertulis ke Lembaga/Unit Organisasi > 10. Tingkat kepercayaan stakeholder baik. Tingkat kepuasan pengguna layanan 4 ≤ x ≤ 4.25 (skala 5)'
                        ],
                        [
                            'level' => [2],
                            'description' => 'Jumlah keluhan secara lisan (dapat didokumentasikan)/tertulis ke Lembaga/Unit Organisasi 3 ≤ x < 5. Tingkat kepuasan pengguna layanan 4 ≤ x ≤ 4.25 (skala 5)'
                        ],
                    ],
                ],
                [
                    'impact_id' => 3,
                    'mng_level' => [
                        [
                            'level' => [0, 1],
                            'description' => 'Pemberitaan negatif yang masif di media sosial yang bersumber dari bukan opinion leader Pemberitaan negatif di media massa lokal. Tingkat kepercayaan stakeholder sedang. Tingkat kepuasan pengguna layanan sebesar 3.75 ≤ x < 4 (skala 5)'
                        ],
                        [
                            'level' => [2],
                            'description' => 'Jumlah keluhan secara lisan (dapat didokumentasikan)/tertulis ke Lembaga/Unit Organisasi > 5. Tingkat kepuasan pengguna layanan 3.75 ≤ x ≤ 4 (skala 5)'
                        ],
                    ],
                ],
                [
                    'impact_id' => 4,
                    'mng_level' => [
                        [
                            'level' => [0, 1],
                            'description' => 'Pemberitaan negatif yang masif di media sosial yang bersumber dari opinion leader Pemberitaan negatif di media massa nasional. Tingkat kepercayaan stakeholder rendah. Tingkat kepuasan pengguna layanan sebesar 3.5 ≤ x < 3.75 (skala 5)'
                        ],
                        [
                            'level' => [2],
                            'description' => 'Pemberitaan negatif di media massa lokal Pemberitaan negatif yang masif di media sosial. Tingkat kepuasan pengguna layanan sebesar 3.5 ≤ x < 3.75 (skala 5)'
                        ],
                    ],
                ],
                [
                    'impact_id' => 5,
                    'mng_level' => [
                        [
                            'level' => [0, 1],
                            'description' => 'Tingkat kepercayaan stakeholder sangat rendah Pemberitaan negatif di media massa internasional. Tingkat kepuasan pengguna layanan < 3.5 (skala 5)'
                        ],
                        [
                            'level' => [2],
                            'description' => 'Pemberitaan negatif di media massa nasional dan internasional. Tingkat kepuasan pengguna layanan < 3.5 (skala 5)'
                        ],
                    ],
                ],
            ]
        ],
        [
            'category_id' => 3,
            'impacts' => [
                [
                    'impact_id' => 1,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => 'Perdata: ≤ 100juta atau Administratif: tergugat merupakan Pejabat Eselon III, IV, dan/atau pejabat yang setara, pejabat fungsional, dan pejabat fungsional umum'
                        ],
                        [
                            'level' => [1],
                            'description' => 'Administratif: tergugat merupakan Pejabat Eselon IV, atau pejabat yang setara, pejabat fungsional, dan pejabat fungsional umum'
                        ],
                        [
                            'level' => [2],
                            'description' => ''
                        ],
                    ],
                ],
                [
                    'impact_id' => 2,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => 'Perdata:100juta < x ≤ 1M atau Administratif: tergugat merupakan Pejabat Eselon II, atau pejabat yang setara'
                        ],
                        [
                            'level' => [1],
                            'description' => 'Perdata: ≤ 100juta atau Administratif: tergugat merupakan Pejabat Eselon III, atau pejabat yang setara'
                        ],
                        [
                            'level' => [2],
                            'description' => ''
                        ],
                    ],
                ],
                [
                    'impact_id' => 3,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => 'Pidana: x ≤ 1 tahun atau Tersangka/terdakwa: Pejabat Eselon III, IV, atau pejabat yang setara, pejabat fungsional, dan pejabat fungsional umum. Perdata: 1M < x < 10M Administratif: tergugat merupakan Pejabat Eselon I, atau pejabat yang setara'
                        ],
                        [
                            'level' => [1],
                            'description' => 'Pidana: x ≤ 1 tahun atau Tersangka/terdakwa: Pejabat Eselon IV, atau pejabat yang setara, pejabat fungsional, dan pejabat fungsional umum. Perdata: 100juta < x ≤ 1M Administratif: tergugat merupakan Pejabat Eselon II, atau pejabat yang setara'
                        ],
                        [
                            'level' => [2],
                            'description' => 'Perdata: ≤ 100 juta Administratif: tergugat merupakan Pejabat Eselon III, IV, atau pejabat yang setara, pejabat fungsional, dan pejabat fungsional umum'
                        ],
                    ],
                ],
                [
                    'impact_id' => 4,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => 'Pidana: 1 < x ≤ 5 tahun atau Tersangka/terdakwa: Pejabat Eselon I, II, atau pejabat yang setara. Perdata: 10M < x < 100M Administratif: tergugat merupakan Kepala'
                        ],
                        [
                            'level' => [1],
                            'description' => 'Pidana: 1 < x ≤ 2 tahun atau Tersangka/terdakwa: Pejabat Eselon II, III, atau pejabat yang setara. Perdata: 1M< x < 10M Administratif: tergugat merupakan Pejabat Eselon I, atau pejabat yang setara'
                        ],
                        [
                            'level' => [2],
                            'description' => 'Pidana: x ≤ 1 tahun atau Tersangka/terdakwa: Pejabat Eselon III, IV, atau pejabat yang setara, pejabat fungsional, dan pejabat fungsional umum. Perdata: 100juta < x ≤ 1M Administratif: tergugat merupakan Pejabat Eselon II, atau pejabat yang setara'
                        ],
                    ],
                ],
                [
                    'impact_id' => 5,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => 'Pidana: x > 5 tahun atauTersangka/terdakwa: KepalaPerdata: x > 100M'
                        ],
                        [
                            'level' => [1],
                            'description' => 'Pidana: x > 2 tahun atau Tersangka/terdakwa: Pejabat Eselon I atau pejabat yang setara. Perdata: x > 10M'
                        ],
                        [
                            'level' => [2],
                            'description' => 'Pidana: x > 1 tahun atau Tersangka/terdakwa: Pejabat Eselon II atau pejabat yang setara. Perdata: x > 1M'
                        ],
                    ],
                ],
            ]
        ],
        [
            'category_id' => 4,
            'impacts' => [
                [
                    'impact_id' => 1,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => 'Ancaman fisik dan/atau psikis'
                        ],
                    ],
                ],
                [
                    'impact_id' => 2,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => 'Gangguan	kesehatan	fisik	ringan	dan/atau Gangguan kesehatan mental ringan'
                        ],
                    ],
                ],
                [
                    'impact_id' => 3,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => 'Gangguan	kesehatan	fisik	sedang	(cedera	tidak permanen) dan/atau Gangguan kesehatan mental sedang'
                        ],
                    ],
                ],
                [
                    'impact_id' => 4,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => 'Gangguan kesehatan fisik berat (kelumpuhan/cacat permanen) dan/atau Gangguan kesehatan mental berat'
                        ],
                    ],
                ],
                [
                    'impact_id' => 5,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => 'Kematian'
                        ],
                    ],
                ],
            ]
        ],
        [
            'category_id' => 5,
            'impacts' => [
                [
                    'impact_id' => 1,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => 'x < 25%'
                        ],
                        [
                            'level' => [1],
                            'description' => 'x < 15%'
                        ],
                        [
                            'level' => [2],
                            'description' => 'x < 10%'
                        ],
                    ],
                ],
                [
                    'impact_id' => 2,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => '25% ≤ x ≤ 50%'
                        ],
                        [
                            'level' => [1],
                            'description' => '15% ≤x ≤ 40%'
                        ],
                        [
                            'level' => [2],
                            'description' => '10% ≤ x ≤ 25%'
                        ],
                    ],
                ],
                [
                    'impact_id' => 3,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => '50% < x ≤ 75%'
                        ],
                        [
                            'level' => [1],
                            'description' => '40% < x ≤ 65%'
                        ],
                        [
                            'level' => [2],
                            'description' => '25% < x ≤ 50%'
                        ],
                    ],
                ],
                [
                    'impact_id' => 4,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => '75% < x ≤ 90%'
                        ],
                        [
                            'level' => [1],
                            'description' => '65% < x ≤ 80%'
                        ],
                        [
                            'level' => [2],
                            'description' => '50% < x ≤ 65%'
                        ],
                    ],
                ],
                [
                    'impact_id' => 5,
                    'mng_level' => [
                        [
                            'level' => [0],
                            'description' => '> 90%'
                        ],
                        [
                            'level' => [1],
                            'description' => '> 80%'
                        ],
                        [
                            'level' => [2],
                            'description' => '> 65%'
                        ],
                    ],
                ],
            ]
        ],
        [
            'category_id' => 6,
            'impacts' => [
                [
                    'impact_id' => 1,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => 'x < 5% dari target kinerja'
                        ],
                    ],
                ],
                [
                    'impact_id' => 2,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => '5% ≤ x ≤ 10% dari target kinerja'
                        ],
                    ],
                ],
                [
                    'impact_id' => 3,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => '10% < x ≤ 20% dari target kinerja'
                        ],
                    ],
                ],
                [
                    'impact_id' => 4,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => '20% < x ≤ 25% dari target kinerja'
                        ],
                    ],
                ],
                [
                    'impact_id' => 5,
                    'mng_level' => [
                        [
                            'level' => [0, 1, 2],
                            'description' => '> 25% dari target kinerja'
                        ],
                    ],
                ],
            ]
        ],
    ],

    // data cluster
    'cluster' => [
        [
            'from'      => 1,
            'to'        => 5,
            'name'      => 'Sangat Rendah',
            'color'     =>  '#00b32c',
        ],
        [
            'from'      => 6,
            'to'        => 10,
            'name'      => 'Rendah',
            'color'     =>  '#1fd537',
        ],
        [
            'from'      => 11,
            'to'        => 15,
            'name'      => 'Sedang',
            'color'     =>  '#ffc537',
        ],
        [
            'from'      => 16,
            'to'        => 19,
            'name'      => 'Tinggi',
            'color'     =>  '#e40010',
        ],
        [
            'from'      => 20,
            'to'        => 25,
            'name'      => 'Sangat Tinggi',
            'color'     =>  '#b3000c',
        ],
    ],

    // index priority
    'priority' => [
        [1, 3, 5, 9, 20],
        [2, 7, 10, 13, 21],
        [4, 8, 14, 17, 22],
        [6, 12, 16, 19, 24],
        [11, 15, 18, 23, 25]
    ],
];
