{{-- Header --}}
<div id="kt_header" class="header {{ Metronic::printClasses('header', false) }}" {{ Metronic::printAttrs('header') }}>

    {{-- Container --}}
    <div class="container-fluid d-flex align-items-center justify-content-between">
        @if (config('layout.header.self.display'))

            @php
                $kt_logo_image = 'logo-light.png';
            @endphp

            @if (config('layout.header.self.theme') === 'light')
                @php $kt_logo_image = 'logo-dark.png' @endphp
            @elseif (config('layout.header.self.theme') === 'dark')
                @php $kt_logo_image = 'logo-light.png' @endphp
            @endif

            {{-- Header Menu --}}
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                @if(config('layout.aside.self.display') == false)
                    <div class="header-logo">
                        <a href="{{ url('/') }}">
                            <img alt="Logo" src="{{ asset('media/logos/'.$kt_logo_image) }}"/>
                        </a>
                    </div>
                @endif

                <div id="kt_header_menu" class="header-menu header-menu-mobile {{ Metronic::printClasses('header_menu', false) }}" {{ Metronic::printAttrs('header_menu') }}>
                    <ul class="menu-nav {{ Metronic::printClasses('header_menu_nav', false) }}">
                        {{ Menu::renderHorMenu($app_item_menu) }}
                    </ul>
                </div>
            </div>

        @else
            <div class="header-menu-wrapper header-menu-wrapper-left">
                {{-- Page Title --}}
                <h5 class="text-dark font-weight-bold my-2 mr-5">
                    {{ @$page_title }}

                    @if (isset($page_description) && config('layout.subheader.displayDesc'))
                        <small>{{ @$page_description }}</small>
                    @endif
                </h5>

                @if (!empty($page_breadcrumbs))
                    {{-- Separator --}}
                    <div class="subheader-separator subheader-separator-ver my-2 mr-4 d-none"></div>

                    {{-- Breadcrumb --}}
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2">
                        <li class="breadcrumb-item"><a href="#"><i class="flaticon2-shelter text-muted icon-1x"></i></a></li>
                        @foreach ($page_breadcrumbs as $k => $item)
                            <li class="breadcrumb-item">
                                <a href="{{ url($item['page']) }}" class="text-muted">
                                    {{ $item['title'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        @endif

        @include('layout.partials.extras._topbar')
    </div>
</div>
