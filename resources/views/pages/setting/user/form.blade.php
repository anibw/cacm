{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('user.update', $data->id) : route('user.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'user_form']) !!}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nama</label>
                    <div class="col-lg-6">
                        {!! Form::text('name', $data->name ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Fullname', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Email</label>
                    <div class="col-lg-6">
                        {!! Form::text('email', $data->email ?? '', ['class'=>'form-control form-control-lg', 'placeholder' => 'Email', 'autocomplete' => 'off', !empty($data->id) ? 'readonly' : '']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Tipe User</label>
                    <div class="col-lg-6">
                        {!! Form::select('user_type', $user_type, $data->user_type ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Password</label>
                    <div class="col-lg-6">
                        {!! Form::password('password', ['class'=>'form-control form-control-lg', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Password (ulang)</label>
                    <div class="col-lg-6">
                        {!! Form::password('password_confirmation', ['class'=>'form-control form-control-lg', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection
