{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => route('risk_impact.update', $category->id . '-' . $impact->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'user_form']) !!}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Kategori</label>
                    {!! Form::hidden('category_id', $category->id) !!}
                    <div class="col-lg-3">
                        {!! Form::text('category', $category->title, ['class' =>'form-control form-control-solid form-control-lg', 'placeholder' => 'Kategori', 'autocomplete' => 'off', 'readonly']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Level Dampak</label>
                    {!! Form::hidden('impact_id', $impact->id) !!}
                    <div class="col-lg-3">
                        {!! Form::text('impact', $impact->id . '-' . $impact->title, ['class' =>'form-control form-control-solid form-control-lg', 'placeholder' => 'Dampak', 'autocomplete' => 'off', 'readonly']) !!}
                    </div>
                </div>
                @foreach ($data as $dt)
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">{{ get_management_level($dt->mng_level); }}</label>
                    <div class="col-lg-9">
                        {!! Form::textarea('mng_level[]', Str::of($dt->description)->toHtmlString(), ['class' =>'form-control form-control-lg', 'placeholder' => 'Keterangan', 'autocomplete' => 'off', 'rows' => 4]) !!}
                    </div>
                </div>
                @endforeach

                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection
