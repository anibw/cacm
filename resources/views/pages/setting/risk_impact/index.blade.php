{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div class="form-group row my-5">
                <label class="col-form-label">Kategori :</label>
                {!! Form::select('category', $category, $category_id, ['class' => 'form-control selectpicker', 'id'=>'category']); !!}
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th rowspan="2" class="text-center align-middle">Level</th>
                            <th colspan="3" class="text-center">Manajemen</th>
                            <th rowspan="2" class="text-center align-middle"><i class="fas fa-cog"></i></th>
                        </tr>
                        <tr>
                            <th class="text-center">Lembaga</th>
                            <th class="text-center">UPR TK-1</th>
                            <th class="text-center">UPR TK-2</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("risk_impact.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
                category_id: function() { return $('#category').find(':selected').val(); },
            },
        },
        columns: [
            {data: 'level', name: 'level', class:"text-center"},
            {data: 'level_0', name: 'level_0'},
            {data: 'level_1', name: 'level_1'},
            {data: 'level_2', name: 'level_2'},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
    });

    $('#category').on('change', function(){
        myTable.draw();
    });
</script>
@endsection