{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom mb-1">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('stakeholder.update', $data->id) : route('stakeholder.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'data_form']) !!}
                {!! Form::hidden('management_id', $uker->id) !!}
                <div class="form-group row">
                    <label class="col-3 col-form-label">Unit Pemilik Risiko (UPR)</label>
                    <label class="col-9 col-form-label">{{ $uker->title }}</label>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Nomor</label>
                    <div class="col-3">
                        {!! Form::number('nomor', $data->nomor ?? '', ['class' =>'form-control', 'placeholder' => 'Nomor', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Unsur</label>
                    <div class="col-3">
                        <select name="status" id="status" class="form-control">
                            <option value="1" {{ !empty($data->status) ? ($data->status == 1 ? 'Selected' : '') : '' }}>Internal</option>
                            <option value="0" {{ !empty($data->status) ? ($data->status == 0 ? 'Selected' : '') : '' }}>Eksternal</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Pemangku Kepentingan</label>
                    <div class="col-9">
                        {!! Form::text('stakeholder', $data->stakeholder ?? '', ['class' =>'form-control ', 'placeholder' => 'Pemangku Kepentingan', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Keterangan</label>
                    <div class="col-9">
                        {!! Form::text('description', $data->description ?? '', ['class' =>'form-control ', 'placeholder' => 'Keterangan', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection
