{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div class="card-title">
                <label class="font-size-h4">Unit Pemilik Risiko (UPR):
                    {!! Form::select('upr', $uker, 1, ['class' => 'form-control selectpicker', 'id'=>'uker']); !!}
                </label>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="" id="lnk_new" class="btn btn-primary font-weight-bolder">
                    <i class="fas fa-layer-group"></i>
                    Data Baru
                </a>
                <!--end::Button-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">No</th>
                            <th>Unit Pemilik Risiko</th>
                            <th>Unsur</th>
                            <th>Pemangku Kepentingan</th>
                            <th>Keterangan</th>
                            <th class="text-center" style="min-width: 70px"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: true,
        // paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("stakeholder.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                uker_id: function() { return $('#uker').find(':selected').val(); },
            },
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'uker', name: 'uker'},
            {data: 'unsur', name: 'unsur'},
            {data: 'stakeholder', name: 'stakeholder'},
            {data: 'description', name: 'description'},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
    });

    var act_create = "{{ route('stakeholder.create') }}";
    $('#lnk_new').attr('href', act_create + '?uker_id=' + $('#uker').find(':selected').val());

    $('#uker').on('change', function(){
        myTable.draw();
        $('#lnk_new').attr('href', act_create + '?uker_id=' + $('#uker').find(':selected').val());
    });

</script>
@endsection