{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('category.update', $data->id) : route('category.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'user_form']) !!}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Judul</label>
                    <div class="col-lg-9">
                        {!! Form::text('title', $data->title ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Judul', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Keterangan</label>
                    <div class="col-lg-9">
                        {!! Form::text('description', $data->description ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Keterangan', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection
