{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">Level</th>
                            <th>Judul</th>
                            <th>Keterangan</th>
                            <th class="text-center"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("impact.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
            },
        },
        columns: [
            {data: 'level', name: 'level', class:"text-center"},
            {data: 'title', name: 'title'},
            {data: 'description', name: 'description'},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
    });

</script>
@endsection