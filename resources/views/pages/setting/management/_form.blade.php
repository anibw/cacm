<!--begin::Modal-->
<div class="modal fade" id="modal_agency" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Satuan Kerja</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form_agency" method="post" accept-charset="UTF-8" enctype="multipart/form-data" class="form-horizontal">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="parent_id" id="parent_id">
                    <input type="hidden" name="mng_level" id="mng_level">
                    <div class="form-group">
                        <label class="font-weight-bold">Satker Induk -> </label>
                        <span id="parent_title" class="font-weight-bold">#</span>
                    </div>
                    <div class="form-group">
                        <label>Satuan Kerja</label>
                        <input type="text" name="title" id="title" class="form-control"  placeholder="Satuan Kerja"/>
                    </div>
                    <div class="form-group">
                        <label>Pemilik Risiko</label>
                        <input type="text" name="owner_name" id="owner_name" class="form-control"  placeholder="Pemilik Risiko"/>
                        <input type="text" name="owner_position" id="owner_position" class="form-control"  placeholder="Jabatan Pemilik Risiko"/>
                    </div>
                    <div class="form-group">
                        <label>Pengelola Risiko</label>
                        <input type="text" name="manager_name" id="manager_name" class="form-control"  placeholder="Pengelola Risiko"/>
                        <input type="text" name="manager_position" id="manager_position" class="form-control"  placeholder="Jabatan Pengelola Risiko"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary font-weight-bold mr-2" onclick="save_satker();">Simpan</button>
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
