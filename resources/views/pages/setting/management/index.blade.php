{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="row">
        <div class="col-lg-6">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Body-->
                <div class="card-body">
                    <div style="border: 1px solid rgb(204, 204, 204); padding: 5px; overflow: auto; width: 100%; max-height: 350px; background-color: rgb(255, 255, 255);">
                        <div id="tr_agency" class="tree-demo">
                        </div>  
                    </div>
                    <div class="card-toolbar text-right py-5">
                        <div class="dropdown dropdown-inline">
                            <a href="#" class="btn btn-primary font-weight-bold mr-2 dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="flaticon2-plus-1"></i>Tambah</a>
                            <div class="dropdown-menu dropdown-menu-md py-5">
                                <ul class="navi navi-hover">
                                    <li class="navi-item">
                                        <a class="navi-link" href="#" onclick="add_satker()">
                                            <span class="navi-icon"><i class="fas fa-home text-primary"></i></span>
                                            <span class="navi-text">Satker</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a class="navi-link" href="#" onclick="add_satker(false)">
                                            <span class="navi-icon"><i class="fas fa-house-user text-primary"></i></span>
                                            <span class="navi-text">Sub Satker</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <a href="#" class="btn btn-primary font-weight-bold mr-2" onclick="edit_satker()"><i class="flaticon-edit"></i>Edit</a>
                        <a href="#" class="btn btn-warning font-weight-bold" onclick="del_satker()"><i class="flaticon2-rubbish-bin"></i>Delete</a>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
        <div class="col-lg-6">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="form-group">
                        <label>Satuan Kerja</label>
                        <input type="text" readonly id="node_title" class="form-control"  placeholder="Satuan Kerja"/>
                    </div>
                    <div class="form-group">
                        <label>Pemilik Risiko</label>
                        <input type="text" readonly id="node_owner" class="form-control"  placeholder="Pemilik Risiko"/>
                        <input type="text" readonly id="node_owner_position" class="form-control"  placeholder="Jabatan Pemilik Risiko"/>
                    </div>
                    <div class="form-group">
                        <label>Pengelola Risiko</label>
                        <input type="text" readonly id="node_manager" class="form-control"  placeholder="Pengelola Risiko"/>
                        <input type="text" readonly id="node_manager_position" class="form-control"  placeholder="Jabatan Pengelola Risiko"/>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
    </div>
    <!--end::Card-->
    @include('pages.setting.management._form')
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/jstree/themes/default/style.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/jstree/jstree.min.js') }}"></script>
<script>
    var node, node_parent;
    var set_mode = 'edit';
    var str_action = "{{ route('satker.store') }}";

    $('#tr_agency').jstree({
        'core' : {
            "themes": {
                "responsive": false
            },
            'data' : {
                "url" : "{{ route('satker.list_tree') }}",
                "dataType" : "json" // needed only if you do not supply JSON headers
            }
        },

    });

    $('#tr_agency').on("changed.jstree", function (e, data) {
        if (data.selected.length) {
            node = data.instance.get_node(data.selected[0]);
            node_parent = data.instance.get_node(node.parent);

            $('#node_title').val(node.text);
            $('#node_owner').val(node.a_attr['owner_name']);
            $('#node_owner_position').val(node.a_attr['owner_position']);
            $('#node_manager').val(node.a_attr['manager_name']);
            $('#node_manager_position').val(node.a_attr['manager_position']);
        }
    });

    function add_satker(is_satker = true)
    {
        var parent_level;
        if (is_satker) {
            // menambah satker
            if (node_parent.id == '#') {
                Swal.fire("", "Tidak dapat lagi menambah data pada level ini", "warning");
                return false;
            } else {
                $('#parent_id').val(node_parent.id);
                $('#parent_title').text(node_parent.text);
                parent_level = node_parent.a_attr['mng_level'];
            }
        } else {
            // menambah sub satker
            if (node.a_attr['mng_level'] < 2) {
                $('#parent_id').val(node.id);
                $('#parent_title').text(node.text);
                parent_level = node.a_attr['mng_level'];
            } else {
                Swal.fire("", "Tidak dapat lagi menambah sub data pada level ini", "warning");
                return false;
            }
        }
        set_mode = 'add';

        $('.modal-title').html('Tambah Satuan Kerja');
        $('#mng_level').val(parent_level+1);
        $('#title').val('');
        $('#owner_name').val('');
        $('#owner_position').val('');
        $('#manager_name').val('');
        $('#manager_position').val('');
        $('#modal_agency').modal('show');
    }

    function edit_satker()
    {
        if (node_parent.id != '#') {
            $('#parent_title').text(node_parent.text);
        } else {
            $('#parent_title').text('#');
        }
        $('#parent_id').val(node_parent.id);
        set_mode = 'edit';

        $('.modal-title').html('Edit Satuan Kerja');
        $('#mng_level').val(node.a_attr['mng_level']);
        $('#title').val(node.text);
        $('#owner_name').val(node.a_attr['owner_name']);
        $('#owner_position').val(node.a_attr['owner_position']);
        $('#manager_name').val(node.a_attr['manager_name']);
        $('#manager_position').val(node.a_attr['manager_position']);

        $('#modal_agency').modal('show');
    }

    function save_satker() {
        if (set_mode == 'edit') {
            $('#form_agency').prepend('<input type="hidden" name="_method" value="PUT">');
            $('#form_agency').prop('action', str_action + '/' + node.id);
        } else {
            $('#form_agency').prop('action', str_action);
        }
        $('#form_agency').submit();
    }

    function del_satker() {
        if (node.children.length == 0) {
            Swal.fire({
                text: "Apakah anda yakin untuk menghapus data ini?",
                icon: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonText: "Ya, Hapus!"
            }).then(function(result) {
                if (result.value) {
                    $('#form_agency').prepend('<input type="hidden" name="_method" value="DELETE">');
                    $('#form_agency').prop('action', str_action + '/' + node.id);
                    $('#form_agency').submit();
                }
            });
            return false;
        } else {
            Swal.fire("", "Data tidak dapat dihapus, karena punya sub-data", "warning");
        }
    }
</script>
@endsection