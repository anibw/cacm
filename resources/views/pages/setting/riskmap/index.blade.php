{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    {{-- Dashboard --}}
    <div class="row">
        <x-chart title="{{ $chart_risk['title'] }}" class="col-lg-6">
            <div id="{{ $chart_risk['name'] }}" class="d-flex justify-content-center"></div>
        </x-chart>
        <div class="col-lg-6">
            <!--begin::Card-->
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Tingkat Risiko</h3>
                    </div>
                </div>
                <!--begin::Body-->
                <div class="card-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                            <thead class="thead-light">
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>Ri</th>
                                    <th>Tingkat Risiko</th>
                                    <th>Nama</th>
                                    <th>Warna</th>
                                    <th>dari ~</th>
                                    <th>sampai</th>
                                    <th class="text-center"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Card-->
        </div>
    </div>
    @include('pages.setting.riskmap._form')
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script src="{{ asset('js/pages/dashboard/chart-general.js') }}"></script>
<script>
    var chart_risk = heat_chart(@json($chart_risk));

    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("cluster.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
            },
        },
        columns: [
            {data: 'id', name: 'id', class:"text-center"},
            {data: 'risk_level', name: 'risk_level'},
            {data: 'title', name: 'title'},
            {data: 'name', name: 'name'},
            {data: 'color', name: 'color'},
            {data: 'from', name: 'from', class:"text-center"},
            {data: 'to', name: 'to', class:"text-center"},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        columnDefs: [
            {
                target: [3, 4],
                visible: false,
            },
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        select: {
            style:    'single',
        },
    });

    var dt_name, dt_from, dt_to, dt_color;

    myTable.on('select', function(e, dt, type, indexes) {
        dt_name = dt.row({selected: true}).data().name;
        dt_from = dt.row({selected: true}).data().from;
        dt_to = dt.row({selected: true}).data().to;
        dt_color = dt.row({selected: true}).data().color;
    });

    function cluster_edit(cluster_id) {
        var str_action = "{{ url('/setting/cluster') }}" + '/' + cluster_id;
        myTable.row(':eq(' + (cluster_id-1) + ')', { page: 'current' }).select();
        $('#modal_name').val(dt_name);
        $('#modal_from').val(dt_from);
        $('#modal_to').val(dt_to);
        $('#modal_color').val(dt_color);
        $('#form_cluster').prop('action', str_action);
        $('#modal_cluster').modal('show');
    }

    $('#modal_from').on('keyup keydown change', function() {
        var modal_from = parseInt($('#modal_from').val());
        var modal_to = parseInt($('#modal_to').val());
        if(modal_from > modal_to) {
            $('#modal_to').val(modal_from);
        }
    })

    $('#modal_to').on('keyup keydown change', function() {
        var modal_from = parseInt($('#modal_from').val());
        var modal_to = parseInt($('#modal_to').val());
        if(modal_from > modal_to) {
            $('#modal_from').val(modal_to);
        }
    })

</script>
@endsection