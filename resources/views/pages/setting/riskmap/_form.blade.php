<!--begin::Modal-->
<div class="modal fade" id="modal_cluster" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Tingkat Risiko</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form id="form_cluster" method="POST" accept-charset="UTF-8" enctype="multipart/form-data" class="form-horizontal">
                @method('PUT')
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tingkat Risiko</label>
                        <input type="text" name="modal_name" id="modal_name" class="form-control"  placeholder="Tingkat Risiko"/>
                    </div>
                    <div class="form-group">
                        <label>Rentang Nilai</label>
                        <div class="input-group">
                            <input type="number" value="1" min="1" max="25" onkeydown="return false" name="modal_from" id="modal_from" class="form-control col-3"  placeholder="dari"/>
                            <span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
                            <input type="number" value="1" min="1" max="25" onkeydown="return false" name="modal_to" id="modal_to" class="form-control col-3"  placeholder="sampai"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Warna</label>
                        <input type="color" name="modal_color" value="" id="modal_color" class="form-control"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary font-weight-bold mr-2">Simpan</button>
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>