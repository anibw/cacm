{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div></div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{ route('period.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="fas fa-layer-group"></i>
                    Data Baru
                </a>
                <!--end::Button-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">No</th>
                            <th>Periode</th>
                            <th>Keterangan</th>
                            <th class="text-center">Active</th>
                            <th class="text-center" style="width: 70px"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        ajax: {
            type: 'POST',
            url: '{{ route("period.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
            },
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'period', name: 'period'},
            {data: 'description', name: 'description'},
            {data: 'aktif', name: 'aktif', class:"text-center"},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
    });

    function set_active(dt_id) {
        Swal.fire({
                text: "Set Aktif data ini?",
                icon: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonText: "Ya, Aktifkan!"
            }).then(function(result) {
                if (result.value) {
                    set_period_active(dt_id).then(msg => {
                        if (msg == 'OK') {
                            myTable.draw();
                        }
                    });
                }
            });
    }

    function set_period_active(data_id) {
        return new Promise((resolve) => {
            $.ajax({
                type: 'POST',
                url: "{{ url('setting/period/set_active') }}",
                dataType: 'json',
                data: {
                    '_token' : '{{ csrf_token() }}',
                    'data_id':  data_id,
                },
                success: function(msg) {
                    resolve(msg);
                }
            });            
        });
    }

</script>
@endsection