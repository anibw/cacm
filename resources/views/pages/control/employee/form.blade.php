{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('employee.update', $data->id) : route('employee.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'employee_form']) !!}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">NIP</label>
                    <div class="col-lg-6">
                        {!! Form::text('nip', $data->nip ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'NIP', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nama</label>
                    <div class="col-lg-6">
                        {!! Form::text('name', $data->name ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Fullname', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Golongan/Ruang</label>
                    <div class="col-lg-6">
                        {!! Form::select('gol_ruang', $gol_ruang, $data->gol_ruang ?? 11, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jabatan Struktural</label>
                    <div class="col-lg-6">
                        {!! Form::text('position', $data->position ?? '', ['class'=>'form-control form-control-lg', 'placeholder' => 'Jabatan Struktural', 'autocomplete' => 'off', !empty($data->id) ? 'readonly' : '']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jabatan Fungsional</label>
                    <div class="col-lg-6">
                        {!! Form::select('auditor_level_id', $auditor_level, $data->auditor_level_id ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection
