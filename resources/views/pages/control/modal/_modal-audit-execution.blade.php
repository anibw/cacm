<!--begin::Modal-->
<div class="modal fade" id="modal_planning" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pelaksanaan Audit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <!--begin: Datatable-->
                <div class="table-responsive">
                    <table id="db_planning" class="table table-bordered table-default" width="100%" cellspacing="0">
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center"><i class="fas fa-cog"></i></th>
                                <th class="text-center">No</th>
                                <th>Kode</th>
                                <th>Jenis Pengawasan</th>
                                <th>Judul Kegiatan</th>
                                <th>Status Penugasan</th>
                                <th>Waktu Perencanaan</th>
                            </tr>
                            </thead>
                    </table>
                </div>
                <!--end: Datatable-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2" onclick="select_data();">Pilih</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
