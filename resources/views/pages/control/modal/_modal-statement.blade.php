<!--begin::Modal-->
<div class="modal fade" id="modal_statement" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pernyataan Risiko</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <!--begin: Datatable-->
                <div class="table-responsive">
                    <table id="db_statement" class="table table-bordered table-default" width="100%" cellspacing="0">
                        <thead class="thead-light">
                            <tr>
                                <th rowspan="2" class="align-middle"><i class="fas fa-cog"></i></th>
                                <th class="text-center align-middle" rowspan="2">No</th>
                                <th rowspan="2" class="align-middle">Pernyataan Risiko</th>
                                <th rowspan="2" class="align-middle">Kategori Risiko</th>
                                <th rowspan="2" class="align-middle">Penyebab</th>
                                <th colspan="4" class="text-center">Nilai Risiko Melekat</th>
                            </tr>
                            <tr>
                                <th class="text-center">K</th>
                                <th class="text-center">D</th>
                                <th class="text-center">P</th>
                                <th class="text-center">Ri</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!--end: Datatable-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2" onclick="select_statement();">Pilih</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
