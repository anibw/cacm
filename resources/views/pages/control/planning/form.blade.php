{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('planning.update', $data->id) : route('planning.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'planning_form']) !!}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Judul Kegiatan</label>
                    <div class="col-lg-9">
                        {!! Form::textarea('title', $data->title ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Judul', 'autocomplete' => 'off', 'rows' => 2]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">PKPT / NON-PKPT</label>
                    <div class="col-lg-6">
                        {!! Form::select('is_pkpt', [1 => 'PKPT', 0 => 'NON PKPT'], $data->is_pkpt ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Status Penugasan</label>
                    <div class="col-lg-6">
                        {!! Form::select('status', $status, $data->status ?? 0, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Waktu Perencanaan</label>
                    <div class="col-lg-6">
                        {!! Form::select('time_plan', $time_plan, $data->time_plan ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jenis Kegiatan</label>
                    <div class="col-lg-6">
                        {!! Form::select('audit_type_id', $audit_type, $data->audit_type_id ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jenis Pengawasan</label>
                    <div class="col-lg-6">
                        {!! Form::select('supervision_type_id', $supervision, $data->supervision_type_id ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection
