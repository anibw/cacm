{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div></div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{ route('auditan.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="fas fa-layer-group"></i>
                    Data Baru
                </a>
                <!--end::Button-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">No</th>
                            <th>Unit Kerja</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Faksimil</th>
                            <th>Email</th>
                            <th class="text-center" style="min-width: 70px"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        ajax: {
            type: 'POST',
            url: '{{ route("auditan.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
            },
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'satker', name: 'satker'},
            {data: 'alamat', name: 'alamat'},
            {data: 'telp', name: 'telp'},
            {data: 'fax', name: 'fax'},
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', class:"text-center"}
        ],
    });
</script>
@endsection