{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('auditan.update', $data->id) : route('auditan.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'auditan_form']) !!}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Satuan Kerja</label>
                    <div class="col-lg-6">
                        {!! Form::text('satker', $data->satker ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Satuan Kerja', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Alamat</label>
                    <div class="col-lg-6">
                        {!! Form::text('alamat', $data->alamat ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Alamat', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nomor Telepon</label>
                    <div class="col-lg-6">
                        {!! Form::text('telp', $data->telp ?? '', ['class'=>'form-control form-control-lg', 'placeholder' => 'Nomor Telepon', 'autocomplete' => 'off', !empty($data->id) ? 'readonly' : '']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Nomor Faksimil</label>
                    <div class="col-lg-6">
                        {!! Form::text('fax', $data->fax ?? '', ['class'=>'form-control form-control-lg', 'placeholder' => 'Nomor Faksimil', 'autocomplete' => 'off', !empty($data->id) ? 'readonly' : '']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Email</label>
                    <div class="col-lg-6">
                        {!! Form::text('email', $data->email ?? '', ['class'=>'form-control form-control-lg', 'placeholder' => 'Email', 'autocomplete' => 'off', !empty($data->id) ? 'readonly' : '']) !!}
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection
