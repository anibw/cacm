{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('report.update', $data->id) : route('report.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'execution_form']) !!}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Judul Kegiatan</label>
                    <div class="col-lg-9">
                        {!! Form::textarea('title', $data->title ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Judul', 'autocomplete' => 'off', 'rows' => 2]) !!}
                    </div>
                </div>
                {{-- <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jenis Perencanaan</label>
                    <div class="col-lg-6">
                        <label class="col-lg-3 col-form-label">{{ $perencanaan }}</label>
                    </div>
                </div> --}}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Status Pelaporan</label>
                    <div class="col-lg-6">
                        {!! Form::select('status', $status, $data->status ?? 0, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                {{-- <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jenis Pengawasan</label>
                    <div class="col-lg-6">
                        <label class="col-lg-3 col-form-label">{{ $pengawasan }}</label>
                    </div>
                </div> --}}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Dokumen Pelaporan (Nomor & Tanggal)</label>
                    <div class="col-9">
                        <div class="form-group row">
                            <div class="col-4">
                                <div class="input-group">
                                    {!! Form::text('report_no', $data->report_no ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Nomor Pelaporan', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="input-group">
                                    <div class="input-group date" id="report_date">
                                        <input type="text" readonly class="form-control" name="report_date" value="{{  $report_date }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ki ki-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('scripts')
<script>
    var arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }

    $('#tanggal_st').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        // endDate: "0d",
        todayHighlight: true,
        templates: arrows
    });

    $('#tmt_awal').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        // endDate: "0d",
        todayHighlight: true,
        templates: arrows
    });

    $('#tmt_akhir').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        // endDate: "0d",
        todayHighlight: true,
        templates: arrows
    });

</script>
@endsection