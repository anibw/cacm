{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @include('pages.control.modal._modal-audit-execution')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div></div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" id="import_data">
                    <i class="fas fa-layer-group"></i>
                    Data Baru
                </a>
                <!--end::Button-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered display nowrap" style="width:100%">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">No</th>
                            <th>Kode</th>
                            <th>Jenis Pengawasan</th>
                            <th>Judul Laporan</th>
                            <th>Nomor Laporan</th>
                            <th>Tgl Laporan</th>
                            <th>Jenis Laporan</th>
                            <th class="text-center" style="min-width: 105px"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
    {!! Form::open(['url' => route('report.store'), 'id' => 'form_create']) !!}
    {!! Form::close() !!}
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        scrollX: true,
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        // paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("report.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
            },
        },
        rowId: 'id',
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'code', name: 'code'},
            {data: 'supervision', name: 'supervision'},
            {data: 'title', name: 'title'},
            {data: 'report_no', name: 'report_no'},
            {data: 'report_date', name: 'report_date'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
    });

    var table_planning = $('#db_planning').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        // searching: false,
        ordering: false,
        ajax: {
            type: 'POST',
            url: '{{ route("execution.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
                req_from: 'report',
            },
        },
        rowId: 'id',
        columns: [
            {data: null, defaultContent: '', class: 'select-checkbox'},
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'code', name: 'code'},
            {data: 'supervision', name: 'supervision'},
            {data: 'title', name: 'title'},
            {data: 'status', name: 'status'},
            {data: 'time_plan', name: 'time_plan'},
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
    });

    function select_data()
    {
        // var data = table_planning.rows({ selected: true }).data();
        var data = table_planning.rows({ selected: true }).ids();
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                $('#form_create').append('<input name="audit_execution_id[]" type="hidden" value="' + data[i] + '">');
            }
            $('#modal_planning').modal('hide');
            $('#form_create').submit();
        } else {
            Swal.fire("", "Belum ada data terpilih", "info");
        }
    }

    $('#import_data').on('click', function() {
        event.preventDefault();
        $('#modal_planning').modal('show');
    })

</script>
@endsection