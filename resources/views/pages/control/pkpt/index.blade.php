{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @include('pages.control.modal._modal-statement')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div></div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <div class="inline-dropdown">
                    <button class="btn btn-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fas fa-layer-group"></i>Data Baru</button>
                    <!--begin::Dropdown Menu-->
                    <div class="dropdown-menu dropdown-menu-sm">
                        <ul class="navi navi-hover">
                            <li class="navi-item">
                                <a class="navi-link" href="{{ route('planning.create') }}">
                                    <span class="navi-icon"><i class="fas fa-magic text-primary"></i></span>
                                    <span class="navi-text">Buat Data</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a class="navi-link" href="#" data-toggle="modal" id="import_data">
                                    <span class="navi-icon"><i class="fas fa-file-import text-primary"></i></span>
                                    <span class="navi-text">Ambil Data</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end::Button-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">No</th>
                            <th>Kode</th>
                            <th>Jenis Pengawasan</th>
                            <th>Judul Kegiatan</th>
                            <th>Status Penugasan</th>
                            <th>Waktu Perencanaan</th>
                            <th class="text-center" style="min-width: 105px"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
    {!! Form::open(['url' => route('planning.store'), 'id' => 'form_create']) !!}
    {!! Form::close() !!}
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        ajax: {
            type: 'POST',
            url: '{{ route("planning.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
            },
        },
        rowId: 'id',
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'code', name: 'code'},
            {data: 'supervision', name: 'supervision'},
            {data: 'title', name: 'title'},
            {data: 'status', name: 'status'},
            {data: 'time_plan', name: 'time_plan'},
            {data: 'action', name: 'action', class:"text-center"}
        ],
    });

    var table_statement = $('#db_statement').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            type: 'POST',
            url: '{{ route("statement.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                uker_id: 1,
                risk_from: 'audit_plan',
            },
        },
        rowId: 'id',
        columns: [
            {data: null, defaultContent: '', class: 'select-checkbox'},
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'risk_statement', name: 'risk_statement'},
            {data: 'category', name: 'category'},
            {data: 'reason', name: 'reason'},
            {data: 'possibility', name: 'possibility', class:"text-center"},
            {data: 'impact', name: 'impact', class:"text-center"},
            {data: 'priority', name: 'priority', class:"text-center"},
            {data: 'risk_level', name: 'risk_level', class:"text-center"},
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        columnDefs: [
            { orderable: true, className: 'reorder', targets: 7 },
            { orderable: false, targets: '_all' },
        ],
    });

    function select_statement()
    {
        // var data = table_statement.rows({ selected: true }).data();
        var data = table_statement.rows({ selected: true }).ids();
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                $('#form_create').append('<input name="statement_id[]" type="hidden" value="' + data[i] + '">');
            }
            $('#modal_statement').modal('hide');
            $('#form_create').submit();
        } else {
            Swal.fire("", "Belum ada data terpilih", "info");
        }
    }

    $('#import_data').on('click', function() {
        event.preventDefault();
        $('#modal_statement').modal('show');
    })

</script>
@endsection