{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('execution.update', $data->id) : route('execution.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'execution_form']) !!}
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Judul Kegiatan</label>
                    <div class="col-lg-9">
                        <label class="col-lg-3 col-form-label">{{ $data->title ?? '' }}</label>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">PKPT / NON-PKPT</label>
                    <div class="col-lg-6">
                        <label class="col-lg-3 col-form-label">{{ $data->is_pkpt == 1 ? 'PKPT' : 'NON PKPT' }}</label>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Status Penugasan</label>
                    <div class="col-lg-6">
                        {!! Form::select('status', $status, $data->status ?? 0, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Waktu Perencanaan</label>
                    <div class="col-lg-6">
                        {!! Form::select('time_plan', $time_plan, $data->time_plan ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jenis Kegiatan</label>
                    <div class="col-lg-6">
                        {!! Form::select('audit_type_id', $audit_type, $data->audit_type_id ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jenis Pengawasan</label>
                    <div class="col-lg-6">
                        {!! Form::select('supervision_type_id', $supervision, $data->supervision_type_id ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Surat Tugas (Nomor & Tanggal)</label>
                    <div class="col-9">
                        <div class="form-group row">
                            <div class="col-4">
                                <div class="input-group">
                                    {!! Form::text('nomor_st', $data->nomor_st ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Nomor ST', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="input-group">
                                    <div class="input-group date" id="tanggal_st">
                                        <input type="text" readonly class="form-control" name="tanggal_st" value="{{  $tanggal_st }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ki ki-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">TMT Surat Tugas</label>
                    <div class="col-9">
                        <div class="form-group row">
                            <div class="col-4">
                                <div class="input-group">
                                    <div class="input-group date" id="tmt_awal">
                                        <input type="text" readonly class="form-control" name="tmt_awal" value="{{ $tmt_awal }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ki ki-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <span class="form-text">TMT Awal</span>
                            </div>
                            <div class="col-4">
                                <div class="input-group">
                                    <div class="input-group date" id="tmt_akhir">
                                        <input type="text" readonly class="form-control" name="tmt_akhir" value="{{ $tmt_akhir }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="ki ki-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <span class="form-text">TMT Akhir</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jumlah Hari</label>
                    <div class="col-9">
                        <div class="form-group row">
                            <div class="col-4">
                                <div class="input-group">
                                    {!! Form::text('jml_hari', $data->jml_hari ?? '', ['class' =>'form-control form-control-lg', 'placeholder' => 'Jumlah Hari', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Jenis Surat Tugas</label>
                    <div class="col-lg-6">
                        {!! Form::select('jenis_st', $jenis_st, $data->jenis_st ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label">Status Penerbitan</label>
                    <div class="col-lg-6">
                        {!! Form::select('status_st', [1 => 'Tepat Waktu', 0 => 'Mundur'], $data->status_st ?? 1, ['class'=>'form-control selectpicker']) !!}
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('scripts')
<script>
    var arrows = {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }

    $('#tanggal_st').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        // endDate: "0d",
        todayHighlight: true,
        templates: arrows
    });

    $('#tmt_awal').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        // endDate: "0d",
        todayHighlight: true,
        templates: arrows
    });

    $('#tmt_akhir').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        // endDate: "0d",
        todayHighlight: true,
        templates: arrows
    });

</script>
@endsection