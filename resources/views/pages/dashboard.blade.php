{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    {{-- Dashboard --}}
    <div class="row">
        <x-chart title="{{ $heat_data_state['title'] }} Melekat" class="col-lg-6">
            <div id="{{ $heat_data_state['name'] }}" class="d-flex justify-content-center"></div>
        </x-chart>
        <x-chart title="{{ $pie_data_state['title'] }} Melekat" class="col-lg-6">
            <div id="{{ $pie_data_state['name'] }}" class="d-flex justify-content-center"></div>
        </x-chart>
        <x-chart title="{{ $heat_data_control['title'] }} Pengendalian" class="col-lg-6">
            <div id="{{ $heat_data_control['name'] }}" class="d-flex justify-content-center"></div>
        </x-chart>
        <x-chart title="{{ $pie_data_control['title'] }} Pengendalian" class="col-lg-6">
            <div id="{{ $pie_data_control['name'] }}" class="d-flex justify-content-center"></div>
        </x-chart>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('js/pages/dashboard/chart-general.js') }}"></script>
<script>
    var heat_chart_state = heat_chart(@json($heat_data_state));
    var pie_chart_state = pie_chart(@json($pie_data_state));

    var heat_chart_control = heat_chart(@json($heat_data_control));
    var pie_chart_control = pie_chart(@json($pie_data_control));
</script>
@endsection