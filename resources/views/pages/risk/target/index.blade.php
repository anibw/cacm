{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div class="card-title">
                <label class="font-size-h4">Unit Pemilik Risiko (UPR):
                    {!! Form::select('upr', $uker, 1, ['class' => 'form-control selectpicker', 'id'=>'uker']); !!}
                </label>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{ route('target.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="fas fa-layer-group"></i>
                    Data Baru
                </a>
                <!--end::Button-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">No</th>
                            <th>UPR</th>
                            <th>Program/Kegiatan</th>
                            <th>Sasaran</th>
                            <th>Tujuan</th>
                            <th class="text-center"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        // paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("target.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                uker_id: function() { return $('#uker').find(':selected').val(); },
            },
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'upr', name: 'upr'},
            {data: 'activity', name: 'activity'},
            {data: 'target', name: 'target'},
            {data: 'goal', name: 'goal'},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
    });

    $('#uker').on('change', function(){
        myTable.draw();
    });

</script>
@endsection