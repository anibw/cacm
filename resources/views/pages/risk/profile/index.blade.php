{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div class="card-title">
                <label class="font-size-h4">Unit Pemilik Risiko (UPR):
                    {!! Form::select('uker', $uker, $uker_id, ['class' => 'form-control selectpicker', 'id'=>'uker']); !!}
                </label>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <div class="inline-dropdown">
                    <button type="button" class="btn btn-primary font-weight-bolder dropdown-toggle mr-3" data-toggle="dropdown">
                        <span class="card-icon"><i class="fas fa-file-export text-white"></i></span>
                        Ekspor..
                    </button>
                    <!--begin::Dropdown Menu-->
                    <div class="dropdown-menu dropdown-menu-sm">
                        <!--begin::Navigation-->
                        <ul class="navi flex-column navi-hover py-2">
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_print">
                                    <span class="navi-icon"><i class="fas fa-print"></i></span>
                                    <span class="navi-text">Printer</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_excel">
                                    <span class="navi-icon"><i class="fas fa-file-excel"></i></span>
                                    <span class="navi-text">Excel</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_pdf">
                                    <span class="navi-icon"><i class="fas fa-file-pdf"></i></span>
                                    <span class="navi-text">PDF</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_copy">
                                    <span class="navi-icon"><i class="fas fa-copy"></i></span>
                                    <span class="navi-text">Copy</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_csv">
                                    <span class="navi-icon"><i class="fas fa-file-csv"></i></span>
                                    <span class="navi-text">CSV</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="inline-dropdown">
                    <button class="btn btn-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fas fa-layer-group"></i>Data Baru</button>
                    <!--begin::Dropdown Menu-->
                    <div class="dropdown-menu dropdown-menu-sm">
                        <ul class="navi navi-hover">
                            <li class="navi-item">
                                <a class="navi-link" href="{{ route('profile.create') }}">
                                    <span class="navi-icon"><i class="fas fa-magic text-primary"></i></span>
                                    <span class="navi-text">Buat Data</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a class="navi-link" href="#">
                                    <span class="navi-icon"><i class="fas fa-file-import text-primary"></i></span>
                                    <span class="navi-text">Import Data</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center">No</th>
                            <th>UPR</th>
                            <th>Nomor</th>
                            <th>Smt</th>
                            <th>Kegiatan</th>
                            <th>Target</th>
                            <th>Indikator</th>
                            <th>Tujuan</th>
                            <th class="text-center" style="min-width: 70px"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        // searching: false,
        ordering: false,
        // paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("profile.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                uker_id: function() { return $('#uker').find(':selected').val(); },
            },
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'uker', name: 'uker'},
            {data: 'number', name: 'number'},
            {data: 'semester', name: 'semester', class:"text-center"},
            {data: 'activity', name: 'activity'},
            {data: 'target', name: 'target'},
            {data: 'indicator', name: 'indicator'},
            {data: 'goal', name: 'goal'},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        columnDefs: [
            {
                target: [2, 5, 6],
                visible: false,
            },
        ],
        // oLanguage: {
        //     sSearch: "cari data"    
        // },
        language: {
            emptyTable: "Data tidak tersedia"
        },
        buttons: [
            'print', 'excel', 'pdf', 'copy', 'csv' 
        ],

    });

    $('#uker').on('change', function(){
        myTable.draw();
    });

    $('#export_print').on('click', function(e) {
        e.preventDefault();
        myTable.button(0).trigger();
    });

    $('#export_excel').on('click', function(e) {
        e.preventDefault();
        myTable.button(1).trigger();
    });

    $('#export_pdf').on('click', function(e) {
        e.preventDefault();
        myTable.button(2).trigger();
    });

    $('#export_copy').on('click', function(e) {
        e.preventDefault();
        myTable.button(3).trigger();
    });

    $('#export_csv').on('click', function(e) {
        e.preventDefault();
        myTable.button(4).trigger();
    });

</script>
@endsection