{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom mb-1">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('profile.update', $data->id) : route('profile.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'data_form']) !!}
                <div class="form-group row">
                    <label class="col-3 col-form-label">Periode Data</label>
                    <label class="col-9 col-form-label">Data Periode Tahun {{ period()->period }}</label>
                    {!! Form::hidden('period_id', period()->id) !!}
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Unit Pemilik Risiko</label>
                    <div class="col-9">
                        {!! Form::select('management_id', $upr, $data->management_id ?? 1, ['class' => 'form-control selectpicker']); !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Nomor</label>
                    <div class="col-3">
                        {!! Form::text('number', $data->number ?? '', ['class' =>'form-control', 'placeholder' => 'Nomor', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Kegiatan</label>
                    <div class="col-9">
                        {!! Form::textarea('activity', $data->activity ?? '', ['class' =>'form-control', 'placeholder' => 'Kegiatan', 'autocomplete' => 'off', 'rows' => 3]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Target</label>
                    <div class="col-9">
                        {!! Form::textarea('target', $data->target ?? '', ['class' =>'form-control', 'placeholder' => 'Target', 'autocomplete' => 'off', 'rows' => 3]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Indikator</label>
                    <div class="col-9">
                        {!! Form::textarea('indicator', $data->indicator ?? '', ['class' =>'form-control', 'placeholder' => 'Indikator', 'autocomplete' => 'off', 'rows' => 3]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Tujuan</label>
                    <div class="col-9">
                        {!! Form::textarea('goal', $data->goal ?? '', ['class' =>'form-control', 'placeholder' => 'Tujuan', 'autocomplete' => 'off', 'rows' => 3]) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Semester</label>
                    <div class="col-3">
                        {!! Form::select('semester', [1 => 'Semester I', 2 => 'Semester II'], 1, ['class' => 'form-control selectpicker']); !!}
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection
