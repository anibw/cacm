<!--begin::Modal-->
<div class="modal fade" id="tmp_statement" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Data Risiko</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-3 col-form-label">Kategori Risiko</label>
                    <div class="col-6">
                        {!! Form::select('category_id', $category, $data->category_id ?? 1, ['class' => 'form-control selectpicker', 'id' => 'category', 'onchange' => 'category_change(this)']); !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Pernyataan Risiko</label>
                    <div class="col-9">
                        {!! Form::textarea('risk_statement', $data->risk_statement ?? '', ['class' =>'form-control', 'placeholder' => 'Pernyataan Risiko', 'rows' => 2, 'autocomplete' => 'off', 'required']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Penyebab</label>
                    <div class="col-9">
                        {!! Form::textarea('reason', $data->reason ?? '', ['class' =>'form-control', 'placeholder' => 'Penyebab', 'rows' => 2, 'autocomplete' => 'off', 'required']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Nilai Risiko Melekat</label>
                    <div class="col-9">
                        <div class="form-group row">
                            <div class="col-3">
                                <div class="input-group">
                                    {!! Form::select('possibility', $possibilities, $data->possibility ?? "", ['class' => 'form-control selectpicker', 'onchange' => 'calculate_score();', 'id' => 'val_possibility', 'disabled', 'required']); !!}
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-icon" data-toggle="modal" onclick="show_modal_possibility()" type="button" title="Pilih Tingkat Kemungkinan"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                                <span class="form-text">Tingkat Kemungkinan (K)</span>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    {!! Form::select('impact', $impacts, $data->impact ?? "", ['class' => 'form-control selectpicker', 'onchange' => 'calculate_score();', 'id' => 'val_impact', 'disabled', 'required']); !!}
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-icon" data-toggle="modal" onclick="show_modal_impact()" type="button" title="Pilih Tingkat Dampak"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                                <span class="form-text">Tingkat Dampak (D)</span>
                            </div>
                            {!! Form::hidden('score', $data->score ?? '', ['id' => 'val_score']) !!}
                            <div class="col-2">
                                {!! Form::number('priority', $data->priority ?? '', ['class' =>'form-control', 'title' => 'Prioritas', 'placeholder' => 'Prioritas', 'autocomplete' => 'off', 'readonly', 'id' => 'val_priority', 'required']) !!}
                                <span class="form-text">Prioritas (P)</span>
                            </div>
                            <div class="col-4">
                                <button type="button" id="btn_risiko" class="btn btn-sm text-white font-weight-bold"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary mr-2" onclick="select_tmp_statement();">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
