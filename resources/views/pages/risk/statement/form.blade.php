{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @include('pages.risk.modal._modal-profile')
    @include('pages.risk.modal._modal-possibility')
    @include('pages.risk.modal._modal-impact')
    <!--begin::Card-->
    <div class="card card-custom mb-1">
        <!--begin::Body-->
        <div class="card-body">
            {!! Form::model($data, ['url' => (!empty($data->id)) ? route('statement.update', $data->id) : route('statement.store'), 'method' => (!empty($data->id)) ? 'PUT' : 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'id' => 'data_form']) !!}
                <div class="form-group row">
                    <label class="col-3 col-form-label">Periode Data</label>
                    <label class="col-9 col-form-label">Data Periode Tahun {{ period()->period }}</label>
                    {!! Form::hidden('period_id', period()->id) !!}
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Unit Pemilik Risiko</label>
                    <label class="col-9 col-form-label" id="uker_name">{{ $data->risk_profile->management->title ?? '' }}</label>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Kegiatan Utama</label>
                    {!! Form::hidden('risk_profile_id', $data->risk_profile_id ?? '', ['id' => 'risk_profile_id']) !!}
                    <div class="input-group col-9">
                        {!! Form::textarea('activity', $data->risk_profile->activity ?? '', ['class' =>'form-control form-control-solid', 'placeholder' => 'Kegiatan Utama', 'rows' => 2, 'autocomplete' => 'off', 'readonly', 'id' => 'risk_activity', 'required']) !!}
                        <div class="input-group-append">
                            <button class="btn btn-primary font-weight-bold" data-toggle="modal" data-target="#modal_profile" type="button" title="Pilih Tujuan Kegiatan Utama">Pilih</button>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Tujuan Kegiatan Utama</label>
                    <div class="col-9">
                        {!! Form::textarea('goal', $data->risk_profile->goal ?? '', ['class' =>'form-control form-control-solid', 'placeholder' => 'Tujuan Kegiatan Utama', 'rows' => 2, 'autocomplete' => 'off', 'readonly', 'id' => 'risk_goal', 'required']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Kategori Risiko</label>
                    <div class="col-6">
                        {!! Form::select('category_id', $category, $data->category_id ?? 1, ['class' => 'form-control selectpicker', 'id' => 'category', 'onchange' => 'category_change(this)']); !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Pernyataan Risiko</label>
                    <div class="col-9">
                        {!! Form::textarea('risk_statement', $data->risk_statement ?? '', ['class' =>'form-control', 'placeholder' => 'Pernyataan Risiko', 'rows' => 2, 'autocomplete' => 'off', 'required']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Penyebab</label>
                    <div class="col-9">
                        {!! Form::textarea('reason', $data->reason ?? '', ['class' =>'form-control', 'placeholder' => 'Penyebab', 'rows' => 2, 'autocomplete' => 'off', 'required']) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-3 col-form-label">Nilai Risiko Melekat</label>
                    <div class="col-9">
                        <div class="form-group row">
                            <div class="col-3">
                                <div class="input-group">
                                    {!! Form::select('possibility', $possibilities, $data->possibility ?? "", ['class' => 'form-control selectpicker', 'onchange' => 'calculate_score();', 'id' => 'val_possibility', 'disabled', 'required']); !!}
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-icon" data-toggle="modal" onclick="show_modal_possibility()" type="button" title="Pilih Tingkat Kemungkinan"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                                <span class="form-text">Tingkat Kemungkinan (K)</span>
                            </div>
                            <div class="col-3">
                                <div class="input-group">
                                    {!! Form::select('impact', $impacts, $data->impact ?? "", ['class' => 'form-control selectpicker', 'onchange' => 'calculate_score();', 'id' => 'val_impact', 'disabled', 'required']); !!}
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-icon" data-toggle="modal" onclick="show_modal_impact()" type="button" title="Pilih Tingkat Dampak"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                                <span class="form-text">Tingkat Dampak (D)</span>
                            </div>
                            {!! Form::hidden('score', $data->score ?? '', ['id' => 'val_score']) !!}
                            <div class="col-2">
                                {!! Form::number('priority', $data->priority ?? '', ['class' =>'form-control', 'title' => 'Prioritas', 'placeholder' => 'Prioritas', 'autocomplete' => 'off', 'readonly', 'id' => 'val_priority', 'required']) !!}
                                <span class="form-text">Nilai Risiko (N)</span>
                            </div>
                            <div class="col-4">
                                <button type="button" id="btn_risiko" class="btn btn-sm text-white font-weight-bold"></button>
                                <span class="form-text">Tingkat Risiko (Ri)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group d-flex flex-wrap flex-center">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary font-weight-bold px-9 py-4 my-3 mx-4']) !!}
                    {!! Form::button('Cancel', ['class' => 'btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-4', 'onclick' => 'history.back()']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var profile_id, goal, uker, activity;
    var possibility_id, impact_id;
    var impact_title;

    var myTable = $('#dbtable_modal').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        // searching: false,
        ordering: false,
        // paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("profile.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                uker_id: function() { return $('#uker').find(':selected').val(); },
            },
        },
        columns: [
            {data: null, defaultContent: '', class: 'select-checkbox'},
            {data: 'id', name: 'id'},
            {data: 'uker', name: 'uker'},
            {data: 'number', name: 'number'},
            {data: 'semester', name: 'semester', class:"text-center"},
            {data: 'activity', name: 'activity'},
            {data: 'target', name: 'target'},
            {data: 'indicator', name: 'indicator'},
            {data: 'goal', name: 'goal'},
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        select: {
            style:    'single',
            selector: 'td:first-child'
        },
        columnDefs: [
            {target: [1, 6, 7],  visible: false}
        ],
    });

    var myTable_possibility = $('#dbtable_possibility').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("possibility.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
            },
        },
        columns: [
            {data: null, defaultContent: '', class: 'select-checkbox'},
            {data: 'level', name: 'level', class:"text-center data-level"},
            {data: 'title', name: 'title'},
            {data: 'tolerance', name: 'tolerance'},
            {data: 'percentage', name: 'percentage'},
            {data: 'frequency', name: 'frequency'},
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        select: {
            style:    'single',
            selector: 'td:first-child'
        },
    });

    var myTable_impact = $('#dbtable_impact').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        searching: false,
        ordering: false,
        paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("risk_impact.list") }}',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
                category_id: function() { return $('#category').find(':selected').val(); },
            },
        },
        columns: [
            {data: null, defaultContent: '', class: 'select-checkbox'},
            {data: 'level', name: 'level', class:"text-center"},
            {data: 'title', name: 'title'},
            {data: 'level_0', name: 'level_0'},
            {data: 'level_1', name: 'level_1'},
            {data: 'level_2', name: 'level_2'},
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        select: {
            style:    'single',
            selector: 'td:first-child'
        },
    });

    $('#uker').on('change', function(){
        myTable.draw();
    });

    function category_change(obj) {
        $('#val_impact').val(0).change();
        myTable_impact.draw();
        impact_title = 'Dampak ' + $(obj).find(':selected').html();
    }

    function calculate_score()
    {
        let possibility = $('#val_possibility').find(':selected').val();
        let impact = $('#val_impact').find(':selected').val();
        let nilai =  possibility * impact;
        $('#val_score').val(nilai);
        if (nilai > 0) {
            get_cluster(possibility, impact).then(msg => {
                $('#btn_risiko').html(msg.name);
                $('#btn_risiko').css({'background-color': msg.color});
                $('#val_priority').val(msg.priority);
            });
        } else {
            $('#btn_risiko').css({'background-color': ''});
            $('#btn_risiko').html('');
            $('#val_priority').val('');
        }
    }

    myTable.on('select', function(e, dt, type, indexes) {
        profile_id = dt.row({selected: true}).data().id;
        uker = dt.row({selected: true}).data().uker;
        activity = dt.row({selected: true}).data().activity;
        goal = dt.row({selected: true}).data().goal;
    });

    function select_submit()
    {
        $('#risk_profile_id').val(profile_id);
        $('#uker_name').text(uker);
        $('#risk_activity').val(activity);
        $('#risk_goal').val(goal);
        $('#modal_profile').modal('hide');
    }

    myTable_possibility.on('select', function(e, dt, type, indexes) {
        possibility_id = dt.row({selected: true}).data().level;
    });

    function select_possibility()
    {
        $('#val_possibility').val(possibility_id).change();
        $('#modal_possibility').modal('hide');
    }
    
    function show_modal_possibility() {
        $('#modal_possibility').modal('show');
    }

    function show_modal_impact() {
        $('#modal_impact_title').text(impact_title);
        $('#modal_impact').modal('show');
    }

    myTable_impact.on('select', function(e, dt, type, indexes) {
        impact_id = dt.row({selected: true}).data().level;
    });

    function select_impact()
    {
        $('#val_impact').val(impact_id).change();
        $('#modal_impact').modal('hide');
    }

    function get_cluster(pos_id, imp_id) {
        return new Promise((resolve) => {
            $.ajax({
                type: 'POST',
                url: "{{ url('setting/cluster/item') }}",
                dataType: 'json',
                data: {
                    '_token' : '{{ csrf_token() }}',
                    'possibility_id':  pos_id,
                    'impact_id': imp_id
                },
                success: function(msg) {
                    resolve(msg);
                }
            });            
        });
    }

    $('#data_form').on('submit', function() {
        $('#val_possibility').prop('disabled', false);
        $('#val_impact').prop('disabled', false);
    });

    calculate_score();
</script>
@endsection