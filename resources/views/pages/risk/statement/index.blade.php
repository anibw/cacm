{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div class="card-title">
                <label class="font-size-h4">Unit Pemilik Risiko (UPR):
                    {!! Form::select('upr', $uker, $uker_id, ['class' => 'form-control selectpicker', 'id'=>'uker']); !!}
                </label>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <button type="button" class="btn btn-primary font-weight-bolder dropdown-toggle mr-3" data-toggle="dropdown">
                    <span class="card-icon"><i class="fas fa-file-export text-white"></i></span>
                    Ekspor..
                </button>
                <!--begin::Dropdown Menu-->
                <div class="dropdown-menu dropdown-menu-sm">
                    <!--begin::Navigation-->
                    <ul class="navi flex-column navi-hover py-2">
                        <li class="navi-item">
                            <a href="#" class="navi-link" id="export_print">
                                <span class="navi-icon"><i class="fas fa-print"></i></span>
                                <span class="navi-text">Printer</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" class="navi-link" id="export_excel">
                                <span class="navi-icon"><i class="fas fa-file-excel"></i></span>
                                <span class="navi-text">Excel</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" class="navi-link" id="export_pdf">
                                <span class="navi-icon"><i class="fas fa-file-pdf"></i></span>
                                <span class="navi-text">PDF</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" class="navi-link" id="export_copy">
                                <span class="navi-icon"><i class="fas fa-copy"></i></span>
                                <span class="navi-text">Copy</span>
                            </a>
                        </li>
                        <li class="navi-item">
                            <a href="#" class="navi-link" id="export_csv">
                                <span class="navi-icon"><i class="fas fa-file-csv"></i></span>
                                <span class="navi-text">CSV</span>
                            </a>
                        </li>
                    </ul>
                </div>
    
                <a href="{{ route('statement.create') }}" class="btn btn-primary font-weight-bolder">
                    <i class="fas fa-layer-group"></i>
                    Data Baru
                </a>
                <!--end::Button-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="dbtable" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center align-middle" rowspan="2">No</th>
                            <th rowspan="2" class="align-middle">Tujuan Kegiatan Utama</th>
                            <th rowspan="2" class="align-middle">Pernyataan Risiko</th>
                            <th rowspan="2" class="align-middle">Kategori Risiko</th>
                            <th rowspan="2" class="align-middle">Penyebab</th>
                            <th colspan="4" class="text-center">Nilai Risiko Melekat</th>
                            <th class="text-center align-middle" rowspan="2" style="min-width: 70px"><i class="fas fa-cog"></i></th>
                        </tr>
                        <tr>
                            <th class="text-center" style="cursor:pointer" title="Kemungkinan">K</th>
                            <th class="text-center" style="cursor:pointer" title="Dampak">D</th>
                            <th class="text-center" title="Nilai Risiko">N</th>
                            <th class="text-center" style="cursor:pointer" title="Tingkat Risiko">Ri</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var myTable = $('#dbtable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        // searching: false,
        // ordering: false,
        // paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("statement.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                uker_id: function() { return $('#uker').find(':selected').val(); },
            },
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'goal', name: 'goal'},
            {data: 'risk_statement', name: 'risk_statement'},
            {data: 'category', name: 'category'},
            {data: 'reason', name: 'reason'},
            {data: 'possibility', name: 'possibility', class:"text-center"},
            {data: 'impact', name: 'impact', class:"text-center"},
            {data: 'priority', name: 'priority', class:"text-center"},
            {data: 'risk_level', name: 'risk_level', class:"text-center"},
            {data: 'action', name: 'action', class:"text-center"}
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        rowReorder: true,
        columnDefs: [
            { orderable: true, className: 'reorder', targets: 7 },
            { orderable: false, targets: '_all' }
        ],
        // dom: 'frtp',
        buttons: [
            'print', 'excel', 'pdf', 'copy', 'csv' 
        ],
    });

    $('#uker').on('change', function(){
        myTable.draw();
    });

    $('#export_print').on('click', function(e) {
        e.preventDefault();
        myTable.button(0).trigger();
    });

    $('#export_excel').on('click', function(e) {
        e.preventDefault();
        myTable.button(1).trigger();
    });

    $('#export_pdf').on('click', function(e) {
        e.preventDefault();
        myTable.button(2).trigger();
    });

    $('#export_copy').on('click', function(e) {
        e.preventDefault();
        myTable.button(3).trigger();
    });

    $('#export_csv').on('click', function(e) {
        e.preventDefault();
        myTable.button(4).trigger();
    });

</script>
@endsection