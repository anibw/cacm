{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    @include('pages.risk.modal._modal-statement')
    <!--begin::Card-->
    <div class="card card-custom">
        <!--begin::Header-->
        <div class="card-header">
            <div class="card-title">
                <label class="font-size-h4">Unit Pemilik Risiko (UPR):
                    {!! Form::select('upr', $uker, $uker_id, ['class' => 'form-control selectpicker', 'id'=>'uker']); !!}
                </label>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="#" class="btn btn-primary font-weight-bolder" data-toggle="modal" id="new_data">
                    <i class="fas fa-layer-group"></i>
                    Data Baru
                </a>
                <!--end::Button-->
            </div>
        </div>
        <!--end::Header-->
        <!--begin::Body-->
        <div class="card-body">
            <!--begin: Datatable-->
            <div class="table-responsive">
                <table id="db_control" class="table table-bordered table-default" width="100%" cellspacing="0">
                    <thead class="thead-light">
                        <tr>
                            <th class="text-center align-middle" rowspan="2">No</th>
                            <th rowspan="2" class="align-middle">Pernyataan Risiko</th>
                            <th colspan="3" class="text-center">Risiko Pengendalian</th>
                            <th rowspan="2" class="align-middle">Respon</th>
                            <th rowspan="2" class="align-middle">Inovasi</th>
                            <th rowspan="2" class="align-middle">Alokasi SD</th>
                            <th colspan="4" class="text-center">Risiko Respon</th>
                            <th class="text-center align-middle" rowspan="2" style="min-width: 70px"><i class="fas fa-cog"></i></th>
                        </tr>
                        <tr>
                            <th class="text-center" style="cursor:pointer" title="Kemungkinan">K</th>
                            <th class="text-center" style="cursor:pointer" title="Dampak">D</th>
                            <th class="text-center" style="cursor:pointer" title="Tingkat Risiko">Ri</th>

                            <th class="text-center" style="cursor:pointer" title="Kemungkinan">K</th>
                            <th class="text-center" style="cursor:pointer" title="Dampak">D</th>
                            <th class="text-center" title="Nilai Risiko">N</th>
                            <th class="text-center" style="cursor:pointer" title="Tingkat Risiko">Ri</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--end: Datatable-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Card-->
    {!! Form::open(['url' => route('response.store'), 'id' => 'form_create']) !!}
    {!! Form::close() !!}
@endsection

@section('styles')
<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
<script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script>
    var table_control = $('#db_control').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        // searching: false,
        // ordering: false,
        // paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("response.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                uker_id: function() { return $('#uker').find(':selected').val(); },
            },
        },
        rowId: 'id',
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', class:"text-center"},
            {data: 'risk_statement', name: 'risk_statement'},
            {data: 'st_possibility', name: 'st_possibility', class:"text-center"},
            {data: 'st_impact', name: 'st_impact', class:"text-center"},
            {data: 'st_risk', name: 'st_risk', class:"text-center"},

            {data: 'response', name: 'response'},
            {data: 'innovation', name: 'innovation'},
            {data: 'resource', name: 'resource'},

            {data: 'possibility', name: 'possibility', class:"text-center"},
            {data: 'impact', name: 'impact', class:"text-center"},
            {data: 'priority', name: 'priority', class:"text-center"},
            {data: 'risk_level', name: 'risk_level', class:"text-center"},

            {data: 'action', name: 'action', class:"text-center"}
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        columnDefs: [
            { orderable: true, className: 'reorder', targets: 10 },
            { orderable: false, targets: '_all' }
        ],
        // dom: 'frtp',
        buttons: [
            'print', 'excel', 'pdf', 'copy', 'csv' 
        ],
    });

    $('#uker').on('change', function(){
        table_control.draw();
    });

    var table_statement = $('#db_statement').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        // searching: false,
        // ordering: false,
        // paging: false,
        ajax: {
            type: 'POST',
            url: '{{ route("statement.list") }}',
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                uker_id: function() { return $('#uker').find(':selected').val(); },
                risk_from: 'respon',
            },
        },
        rowId: 'id',
        columns: [
            {data: null, defaultContent: '', class: 'select-checkbox'},
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'risk_statement', name: 'risk_statement'},
            {data: 'category', name: 'category'},
            {data: 'reason', name: 'reason'},
            {data: 'possibility', name: 'possibility', class:"text-center"},
            {data: 'impact', name: 'impact', class:"text-center"},
            {data: 'priority', name: 'priority', class:"text-center"},
            {data: 'risk_level', name: 'risk_level', class:"text-center"},
        ],
        language: {
            emptyTable: "Data tidak tersedia"
        },
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        columnDefs: [
            { orderable: true, className: 'reorder', targets: 7 },
            { orderable: false, targets: '_all' },
        ],
    });

    function select_statement()
    {
        // var data = table_statement.rows({ selected: true }).data();
        var data = table_statement.rows({ selected: true }).ids();
        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                $('#form_create').append('<input name="statement_id[]" type="hidden" value="' + data[i] + '">');
            }
            $('#modal_statement').modal('hide');
            $('#form_create').submit();
        } else {
            Swal.fire("", "Belum ada data terpilih", "info");
        }
    }

    $('#new_data').on('click', function() {
        event.preventDefault();
        $('#modal_statement').modal('show');
    })
</script>
@endsection