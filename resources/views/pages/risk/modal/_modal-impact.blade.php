    <!--begin::Modal-->
    <div class="modal fade" id="modal_impact" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_impact_title">Dampak</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table id="dbtable_impact" class="table table-bordered table-default" width="100%" cellspacing="0">
                            <thead class="thead-light">
                                <tr>
                                    <th class="text-center"><i class="fas fa-cog"></i></th>
                                    <th class="text-center" >Level</th>
                                    <th>Judul</th>
                                    <th>Lembaga</th>
                                    <th>UPR TK-1</th>
                                    <th>UPR TK-2</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary mr-2" onclick="select_impact();">Pilih</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal-->
