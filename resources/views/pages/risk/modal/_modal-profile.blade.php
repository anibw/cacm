    <!--begin::Modal-->
    <div class="modal fade" id="modal_profile" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tujuan Kegiatan Utama</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-3 col-form-label">Unit Pemilik Risiko</label>
                        <div class="col-9">
                            {!! Form::select('uker', $uker, 1, ['class' => 'form-control selectpicker', 'id'=>'uker']); !!}
                        </div>
                    </div>
                    <!--begin: Datatable-->
                    <div class="table-responsive">
                        <table id="dbtable_modal" class="table table-bordered table-default" width="100%" cellspacing="0">
                            <thead class="thead-light">
                                <tr>
                                    <th class="text-center"><i class="fas fa-cog"></i></th>
                                    <th>ID</th>
                                    <th>UPR</th>
                                    <th>Nomor</th>
                                    <th>Semester</th>
                                    <th>Kegiatan</th>
                                    <th>Target</th>
                                    <th>Indikator</th>
                                    <th>Tujuan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!--end: Datatable-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary mr-2" onclick="select_submit();">Pilih</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal-->
